package com.fortuna.accessControl.controller;


import com.fortuna.accessControl.service.IUmRoleService;
import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.controller.FortunaControllerBase;
import com.fortuna.core.dto.accessControl.requestDto.UmRoleRequestDto;
import com.fortuna.core.dto.accessControl.responseDto.UmRoleResponseDto;
import com.fortuna.core.utils.impl.FortunaBeanMapperImpl;
import com.fortuna.database.entity.usermanagement.UmRoleEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil on 5/13/2018.
 */
@RestController
@RequestMapping(UmRoleController.BASE_URL)
public class UmRoleController extends FortunaControllerBase {
    public static final String BASE_URL = WebResourceConstant.UserManagement.UM_ROLE;

    public UmRoleController(IUmRoleService iUmRoleService) {
        super(iUmRoleService, new FortunaBeanMapperImpl(UmRoleEntity.class, UmRoleRequestDto.class), new FortunaBeanMapperImpl(UmRoleEntity.class, UmRoleResponseDto.class));
    }
}
