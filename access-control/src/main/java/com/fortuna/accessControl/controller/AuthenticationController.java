package com.fortuna.accessControl.controller;


import com.fortuna.accessControl.security.IGQBToken;
import com.fortuna.accessControl.service.IAuthenticationService;
import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.dto.accessControl.requestDto.UmChangePasswordRequestDto;
import com.fortuna.core.dto.accessControl.requestDto.UmUserLoginRequestDto;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.core.utils.FortunaGlobalSettingUtils;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by anil on 5/14/18.
 */
@RestController
@RequestMapping(AuthenticationController.BASE_URL)
public class AuthenticationController {
    public static final String BASE_URL = WebResourceConstant.UserManagement.USER_MANAGEMENT;

    private final IGQBToken igqbToken;

    private final IAuthenticationService authenticationService;


    @Autowired
    public AuthenticationController(IGQBToken igqbToken, IAuthenticationService authenticationService) {
        this.igqbToken = igqbToken;
        this.authenticationService = authenticationService;
    }

    @PostMapping(WebResourceConstant.UserManagement.UM_AUTHENTICATE)
    public ResponseEntity<FortunaResponseObj> authenticateUser(@RequestBody @Valid UmUserLoginRequestDto userLoginRequestDto) {
        UmUserEntity umUserEntity = authenticationService.authenticate(userLoginRequestDto.getUserName(), userLoginRequestDto.getPassword());

        //Check login attempt if it is greater then equal to max login attempt then throw exception
        if (umUserEntity.getLoginAttempt() >= Long.valueOf(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.MAX_LOGIN_ATTEMPT))) {
            throw new FortunaException("Sorry!! Your user name '" + umUserEntity.getUserName() + "' is suspended contact to administrator");
        }
        if (!umUserEntity.isPasswordMatch()) {
            throw new FortunaException("Sorry!! Your user name or  password doesn't match. remaining login attempt '" + (Long.valueOf(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.MAX_LOGIN_ATTEMPT)) - umUserEntity.getLoginAttempt()) + "'");
        }

        String token = igqbToken.generateToken(umUserEntity);
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(token).build(), HttpStatus.OK);
    }

    @PostMapping(WebResourceConstant.UserManagement.CHANGE_PASSWORD)
    public ResponseEntity<FortunaResponseObj> changePassword(@RequestBody @Valid UmChangePasswordRequestDto umChangePasswordRequestDto) {
        if (umChangePasswordRequestDto.getNewPassword().equals(umChangePasswordRequestDto.getConfirmPassword())) {
            authenticationService.changePassword(umChangePasswordRequestDto.getOldPassword(), umChangePasswordRequestDto.getNewPassword());
        } else {
            throw new FortunaException("Confirmed Password Didn't match With New Password");
        }

        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().message("Password has been Changed Successfully.").build(), HttpStatus.OK);
    }


}
