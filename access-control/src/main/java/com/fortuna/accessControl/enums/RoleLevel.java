package com.fortuna.accessControl.enums;

/**
 * Created by anil on 7/6/17.
 */
public enum RoleLevel {
    /**
     * Level_0 = Super Admin
     * Level_1 = Administrator
     * Level_2 and above  = Users
     */
    LEVEL_0(0),
    LEVEL_1(1),
    LEVEL_2(2),
    LEVEL_3(3),
    LEVEL_4(4),
    LEVEL_5(5),
    LEVEL_6(6),
    LEVEL_7(7),
    LEVEL_8(8),
    LEVEL_9(9),
    LEVEL_10(10),
    LEVEL_11(11),
    LEVEL_12(12),
    LEVEL_13(13);
    private Integer level;

    RoleLevel(Integer level) {
        this.level = level;
    }

    /**
     * Get RoleLevel Enum from its value
     *
     * @param level
     * @return RoleLevel
     */
    public static RoleLevel getByLevel(Integer level) {
        for (int i = 0; i < RoleLevel.values().length; i++) {
            if (RoleLevel.values()[i].getLevel().equals(level))
                return RoleLevel.values()[i];
        }
        return null;
    }

    public Integer getLevel() {
        return this.level;
    }


}
