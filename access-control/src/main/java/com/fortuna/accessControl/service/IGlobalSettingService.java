package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.GlobalSettingEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IGlobalSettingService extends ICrudService<GlobalSettingEntity, Long> {
}
