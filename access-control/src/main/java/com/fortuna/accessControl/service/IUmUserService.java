package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.usermanagement.UmUserEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IUmUserService extends ICrudService<UmUserEntity, Long> {
}
