package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.SysApplicationLicenseEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface ISysApplicationLicenseService extends ICrudService<SysApplicationLicenseEntity, Long> {
}
