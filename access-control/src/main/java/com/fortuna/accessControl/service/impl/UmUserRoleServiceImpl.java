package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.IUmUserRoleService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.usermanagement.UmUserRoleEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class UmUserRoleServiceImpl extends CrudServiceImpl<UmUserRoleEntity, Long> implements IUmUserRoleService {

    public UmUserRoleServiceImpl(IUmUserRoleRepository umUserRoleRepository) {
        super(umUserRoleRepository);
    }
}
