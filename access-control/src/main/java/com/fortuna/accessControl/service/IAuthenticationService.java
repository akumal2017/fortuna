package com.fortuna.accessControl.service;


import com.fortuna.database.entity.usermanagement.UmUserEntity;

/**
 * Created by admin on 5/14/2018.
 */
public interface IAuthenticationService {
    UmUserEntity authenticate(String userName, String password);

    Boolean changePassword(String oldPassword, String newPassword);

}
