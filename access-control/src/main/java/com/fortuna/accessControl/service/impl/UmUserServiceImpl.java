package com.fortuna.accessControl.service.impl;


import com.fortuna.accessControl.service.IUmUserService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class UmUserServiceImpl extends CrudServiceImpl<UmUserEntity, Long> implements IUmUserService {

    public UmUserServiceImpl(IUmUserRepository umUserRepository) {
        super(umUserRepository);
    }
}
