package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.IUmCompanyService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.usermanagement.UmCompanyEntity;
import com.fortuna.database.repository.accesscontrol.IUmCompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class UmCompanyServiceImpl extends CrudServiceImpl<UmCompanyEntity, Long> implements IUmCompanyService {

    public UmCompanyServiceImpl(IUmCompanyRepository umCompanyRepository) {
        super(umCompanyRepository);
    }
}
