package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.SysFormsEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface ISysFormsService extends ICrudService<SysFormsEntity, Long> {
}
