package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.usermanagement.UmRoleEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IUmRoleService extends ICrudService<UmRoleEntity, Long> {
}
