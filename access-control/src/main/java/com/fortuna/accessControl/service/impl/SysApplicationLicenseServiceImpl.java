package com.fortuna.accessControl.service.impl;


import com.fortuna.accessControl.service.ISysApplicationLicenseService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.gqbsystem.SysApplicationLicenseEntity;
import com.fortuna.database.repository.fortunasystem.ISysApplicationLicenseRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class SysApplicationLicenseServiceImpl extends CrudServiceImpl<SysApplicationLicenseEntity, Long> implements ISysApplicationLicenseService {

    public SysApplicationLicenseServiceImpl(ISysApplicationLicenseRepository iSysApplicationLicenseRepository) {
        super(iSysApplicationLicenseRepository);
    }
}
