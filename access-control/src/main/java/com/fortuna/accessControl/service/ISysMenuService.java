package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.SysMenuEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface ISysMenuService extends ICrudService<SysMenuEntity, Long> {
}
