package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.SysConfigCustomPrivilegeEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface ISysConfigCustomPrivilegeService extends ICrudService<SysConfigCustomPrivilegeEntity, Long> {
}
