package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.security.IGQBPasswordEncoder;
import com.fortuna.accessControl.security.impl.AuthenticationValidator;
import com.fortuna.accessControl.service.IAuthenticationService;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaTokenModel;
import com.fortuna.core.utils.FortunaTokenUtils;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRepository;
import org.springframework.stereotype.Service;

/**
 * Created by admin on 5/14/2018.
 */
@Service
public class AuthenticationServiceImpl implements IAuthenticationService {
    private IUmUserRepository umUserRepository;
    private IGQBPasswordEncoder passwordEncoder;
    private AuthenticationValidator authenticationValidator;

    public AuthenticationServiceImpl(IUmUserRepository umUserRepository, IGQBPasswordEncoder passwordEncoder, AuthenticationValidator authenticationValidator) {
        this.umUserRepository = umUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationValidator = authenticationValidator;
    }

    @Override
    public UmUserEntity authenticate(String userName, String password) {
        UmUserEntity userToAuthenticate = umUserRepository.findByUserName(userName);
        System.out.println("password = " + passwordEncoder.encrypt(password));
        if (passwordEncoder.match(password, userToAuthenticate.getPassword())) {
            userToAuthenticate.setPasswordMatch(true);
        }
        return authenticationValidator.authenticate(userToAuthenticate, this.umUserRepository, password);
    }

    @Override
    public Boolean changePassword(String oldPassword, String newPassword) {
        FortunaTokenModel fortunaTokenModel = FortunaTokenUtils.getGQBTokenModel();
        UmUserEntity umUserEntity = umUserRepository.findOne(fortunaTokenModel.getUserId());
        if (passwordEncoder.match(oldPassword, umUserEntity.getPassword())) {
            umUserEntity.setPassword(passwordEncoder.encrypt(newPassword));
            umUserRepository.update(umUserEntity);
            return true;
        }
        throw new FortunaException("Old Password Didn't match.");
    }
}
