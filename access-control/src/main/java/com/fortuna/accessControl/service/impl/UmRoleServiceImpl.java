package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.IUmRoleService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.usermanagement.UmRoleEntity;
import com.fortuna.database.repository.accesscontrol.IUmRoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class UmRoleServiceImpl extends CrudServiceImpl<UmRoleEntity, Long> implements IUmRoleService {

    public UmRoleServiceImpl(IUmRoleRepository umRoleRepository) {
        super(umRoleRepository);
    }
}
