package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.usermanagement.UmUserRoleEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IUmUserRoleService extends ICrudService<UmUserRoleEntity, Long> {
}
