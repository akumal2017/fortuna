package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.ISysFormsService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.gqbsystem.SysFormsEntity;
import com.fortuna.database.repository.fortunasystem.ISysFormsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class SysFormsServiceImpl extends CrudServiceImpl<SysFormsEntity, Long> implements ISysFormsService {

    public SysFormsServiceImpl(ISysFormsRepository iSysFormsRepository) {
        super(iSysFormsRepository);
    }
}
