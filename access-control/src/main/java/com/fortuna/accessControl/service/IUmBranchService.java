package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.usermanagement.UmBranchEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IUmBranchService extends ICrudService<UmBranchEntity, Long> {
}
