package com.fortuna.accessControl.service.impl;


import com.fortuna.accessControl.service.ISysMenuService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.gqbsystem.SysMenuEntity;
import com.fortuna.database.repository.fortunasystem.ISysMenuRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class SysMenuServiceImpl extends CrudServiceImpl<SysMenuEntity, Long> implements ISysMenuService {

    public SysMenuServiceImpl(ISysMenuRepository iSysMenuRepository) {
        super(iSysMenuRepository);
    }
}
