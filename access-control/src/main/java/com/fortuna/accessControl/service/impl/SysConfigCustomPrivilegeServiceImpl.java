package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.ISysConfigCustomPrivilegeService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.gqbsystem.SysConfigCustomPrivilegeEntity;
import com.fortuna.database.repository.fortunasystem.ISysConfigCustomPrivilegeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class SysConfigCustomPrivilegeServiceImpl extends CrudServiceImpl<SysConfigCustomPrivilegeEntity, Long> implements ISysConfigCustomPrivilegeService {

    public SysConfigCustomPrivilegeServiceImpl(ISysConfigCustomPrivilegeRepository iSysConfigCustomPrivilegeRepository) {
        super(iSysConfigCustomPrivilegeRepository);
    }
}
