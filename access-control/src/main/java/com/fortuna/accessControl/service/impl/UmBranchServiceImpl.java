package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.IUmBranchService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.usermanagement.UmBranchEntity;
import com.fortuna.database.repository.accesscontrol.IUmBranchRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class UmBranchServiceImpl extends CrudServiceImpl<UmBranchEntity, Long> implements IUmBranchService {

    public UmBranchServiceImpl(IUmBranchRepository umBranchRepository) {
        super(umBranchRepository);
    }
}
