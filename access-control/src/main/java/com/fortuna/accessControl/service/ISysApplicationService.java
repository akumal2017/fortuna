package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.gqbsystem.SysApplicationEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface ISysApplicationService extends ICrudService<SysApplicationEntity, Long> {
}
