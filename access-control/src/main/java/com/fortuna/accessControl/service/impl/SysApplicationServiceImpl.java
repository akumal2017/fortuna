package com.fortuna.accessControl.service.impl;

import com.fortuna.accessControl.service.ISysApplicationService;
import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.gqbsystem.SysApplicationEntity;
import com.fortuna.database.repository.fortunasystem.ISysApplicationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/13/2018.
 */
@Service
@Transactional
public class SysApplicationServiceImpl extends CrudServiceImpl<SysApplicationEntity, Long> implements ISysApplicationService {

    public SysApplicationServiceImpl(ISysApplicationRepository iSysApplicationRepository) {
        super(iSysApplicationRepository);
    }
}
