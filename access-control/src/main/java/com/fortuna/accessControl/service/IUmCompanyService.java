package com.fortuna.accessControl.service;


import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.usermanagement.UmCompanyEntity;

/**
 * Created by admin on 5/13/2018.
 */
public interface IUmCompanyService extends ICrudService<UmCompanyEntity, Long> {
}
