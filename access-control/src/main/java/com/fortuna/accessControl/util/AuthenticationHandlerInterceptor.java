package com.fortuna.accessControl.util;


import com.fortuna.accessControl.security.IGQBToken;
import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.utils.FortunaStringUtils;
import com.fortuna.core.utils.FortunaTokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anil on 5/13/17.
 */
public class AuthenticationHandlerInterceptor extends HandlerInterceptorAdapter {


    private static List<String> authorizationFreeuriList = new ArrayList<>();

    static {
        authorizationFreeuriList.add("/auth");
        authorizationFreeuriList.add("/upload");
        authorizationFreeuriList.add("/display");
        authorizationFreeuriList.add("/food");
        authorizationFreeuriList.add("/orderfood/neworder");
        authorizationFreeuriList.add("/send/order");
        authorizationFreeuriList.add("/socket");
        authorizationFreeuriList.add("/app");

    }

    @Autowired
    private IGQBToken igqbToken;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
            return true;
        }
        String uri = request.getRequestURI();
//        System.out.println("uri = " + uri);
        String accessToken;
        String origin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Headers", "*");
        System.out.println("request Uri = " + uri);
        if (isAuthFreeUri(uri)) {
            System.out.println("isAuthFreeUri() = returing true from authfree ");
            return true;
        }
        accessToken = request.getHeader(WebResourceConstant.AUTHORIZATION_HEADER);
        if (FortunaStringUtils.isNull(accessToken) && !isAuthFreeUri(uri)) {
            throw new FortunaException("Unauthorized access!!");
        }
//        String formId = request.getHeader(WebResourceConstant.FORM_HEADER);
//        String applicationId = request.getHeader(WebResourceConstant.APPLICATION_HEADER);
        if (FortunaStringUtils.isNotNull(accessToken)) {
            FortunaTokenUtils.setGQBTokenModel(igqbToken.parseToken(accessToken));
        }

//        FortunaTokenModel fortunaTokenModel = FortunaTokenUtils.getGQBTokenModel();
////        fortunaTokenModel.setFormId(FortunaStringUtils.isNotNull(formId) ? Long.parseLong(formId) : null);
////        fortunaTokenModel.setApplicationId(FortunaStringUtils.isNotNull(applicationId) ? Long.parseLong(applicationId) : null);
        return true;
    }

    private boolean isAuthFreeUri(String uri) {
        if (FortunaStringUtils.isNull(uri)) return false;
        for (String authFreeUri : authorizationFreeuriList) {
            System.out.println("each-authFreeUri = " + authFreeUri);
            if (uri.contains(authFreeUri)) {
                System.out.println("check-authFreeUri = uri " + uri);
                System.out.println("check-authFreeUri = authFreeUri " + authFreeUri);
                return true;
            }
        }
        return false;
    }


}
