package com.fortuna.accessControl.security.impl;


import com.fortuna.accessControl.security.IGQBPasswordEncoder;
import com.fortuna.core.enums.RecordState;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.utils.FortunaDateUtils;
import com.fortuna.core.utils.FortunaGlobalSettingUtils;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import com.fortuna.database.entity.usermanagement.UmUserRoleEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRepository;
import org.springframework.stereotype.Component;

/**
 * Created by Anil on 5/14/18.
 */
@Component
public class AuthenticationValidator extends AuthenticationTemplate {

    private final IGQBPasswordEncoder passwordEncryption;

//    private final SystemResourceUtils systemResourceUtils;

    public AuthenticationValidator(IGQBPasswordEncoder passwordEncryption) {
        this.passwordEncryption = passwordEncryption;
    }

    public static Boolean checkAccessTime(UmUserRoleEntity umUserRoleEntity) {
        if (umUserRoleEntity.getTimeLimit()) {
            if (FortunaDateUtils.getCurrentDateIntoTimeFormat().equals(FortunaDateUtils.getCurrentTime(FortunaDateUtils.convertSqlTimeIntoStringTime(umUserRoleEntity.getFromTime()))) || FortunaDateUtils.getCurrentDateIntoTimeFormat().after(FortunaDateUtils.getCurrentTime(FortunaDateUtils.convertSqlTimeIntoStringTime(umUserRoleEntity.getFromTime())))) {
                if (FortunaDateUtils.getCurrentDateIntoTimeFormat().equals(FortunaDateUtils.getCurrentTime(FortunaDateUtils.convertSqlTimeIntoStringTime(umUserRoleEntity.getToTime()))) || FortunaDateUtils.getCurrentDateIntoTimeFormat().before(FortunaDateUtils.getCurrentTime(FortunaDateUtils.convertSqlTimeIntoStringTime(umUserRoleEntity.getToTime())))) {
                    return true;
                } else {
                    throw new FortunaException("Sorry!! you are not authorised to perform the request !!.");
                }
            } else {
                throw new FortunaException("Sorry!! you are not authorised to perform the request !!.");
            }
        }
        return false;
    }

    @Override
    UmUserEntity validateUser(UmUserEntity umUserEntity) {
        //Check criteria of logged in user

        if (umUserEntity == null) {
            throw new FortunaException("Sorry!! User Not Found");
        }

        if (umUserEntity.getIsSuspended()) {
            throw new FortunaException("Sorry!! Your user name '" + umUserEntity.getUserName() + "' is suspended contact to administrator");
        }
        return umUserEntity;
    }

    @Override
    UmUserEntity loginAttempt(UmUserEntity umUserEntity, IUmUserRepository umUserRepository, String password) {
        //Check Login attempt if equal or greater than three then suspend the user
        String maxLoginSetting = FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.MAX_LOGIN_ATTEMPT);
        if (umUserEntity.getLoginAttempt() != null && umUserEntity.getLoginAttempt() >= Long.valueOf(maxLoginSetting)) {
            umUserEntity.setIsSuspended(true);//Suspend the user
            return umUserEntity;
        }
        //Update wrong attempt by 1 in each request
        umUserEntity.setLoginAttempt(umUserEntity.getLoginAttempt() + 1);
        umUserEntity = umUserRepository.update(umUserEntity);
        if (!passwordEncryption.match(password, umUserEntity.getPassword())) {
            umUserEntity.setPasswordMatch(false);
            if (umUserEntity.getLoginAttempt() >= Long.valueOf(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.MAX_LOGIN_ATTEMPT))) {
                umUserEntity.setIsSuspended(true);
                umUserEntity = umUserRepository.update(umUserEntity);
            }
            return umUserEntity;
        }
        if (umUserEntity.getRecordState().equals(RecordState.ACTIVE.getStatus())) {
            if (umUserEntity.isPasswordMatch()) {
                umUserEntity.setLoginAttempt(0);
                umUserEntity = umUserRepository.update(umUserEntity);
            }
        } else {
            throw new FortunaException("Sorry!! Your user name '" + umUserEntity.getUserName() + "' is inactive contact to administrator");
        }
        return umUserEntity;
    }

    @Override
    UmUserEntity checkUserConfiguration(UmUserEntity umUserEntity) {
        UmUserRoleEntity umUserRoleEntity = umUserEntity.getUmUserRoleEntity();
        if (umUserRoleEntity != null) {
            if (umUserRoleEntity.getDateLimit()) {
                if (FortunaDateUtils.getCurrentDate().equals(umUserRoleEntity.getFromDate()) || FortunaDateUtils.getCurrentDate().after(umUserRoleEntity.getFromDate())) {
                    if (FortunaDateUtils.getCurrentDate().equals(umUserRoleEntity.getToDate()) || FortunaDateUtils.getCurrentDate().before(umUserRoleEntity.getToDate())) {
                        if (umUserRoleEntity.getTimeLimit()) {
                            if (checkAccessTime(umUserRoleEntity)) {
                                return umUserEntity;
                            }
                        }
                    } else {
                        throw new FortunaException("Sorry!! you are not authorised to perform the request !!.");
                    }
                } else {
                    throw new FortunaException("Sorry!! you are not authorised to perform the request !!.");
                }
            }
            if (checkAccessTime(umUserRoleEntity)) {
                return umUserEntity;
            }

        }
        return umUserEntity;
    }
}
