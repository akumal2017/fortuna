package com.fortuna.accessControl.security.impl;

import com.fortuna.accessControl.security.IGQBPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Created by Anil on 5/14/18.
 */
@Component
public class BCryptPasswordImpl implements IGQBPasswordEncoder {

    private PasswordEncoder passwordEncoder;

    public BCryptPasswordImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;

    }

    @Override
    public String encrypt(String password) {
        return passwordEncoder.encode(password);
    }


    @Override
    public Boolean match(String rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
