package com.fortuna.accessControl.security.impl;


import com.fortuna.database.entity.usermanagement.UmUserEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRepository;

/**
 * Created by Anil on 5/14/18.
 */
public abstract class AuthenticationTemplate {


    abstract UmUserEntity validateUser(UmUserEntity umUserEntity);

    abstract UmUserEntity loginAttempt(UmUserEntity umUserEntity, IUmUserRepository umUserRepository, String password);

    abstract UmUserEntity checkUserConfiguration(UmUserEntity umUserEntity);

    public final UmUserEntity authenticate(UmUserEntity umUserEntity, IUmUserRepository umUserRepository, String password) {
        validateUser(umUserEntity);
        loginAttempt(umUserEntity, umUserRepository, password);
        if (umUserEntity.isPasswordMatch()) {
            checkUserConfiguration(umUserEntity);
        }
        return umUserEntity;
    }
}
