package com.fortuna.accessControl.security;


import com.fortuna.core.model.FortunaTokenModel;
import com.fortuna.database.entity.usermanagement.UmUserEntity;

/**
 * Created by Anil on 5/14/18.
 */
public interface IGQBToken {
    /**
     * This method is used to generate token
     *
     * @param umUserEntity which contain system user information
     * @return token
     */
    String generateToken(UmUserEntity umUserEntity);

    /**
     * This method  is used to parse token
     *
     * @param token
     * @return TokenInfoModel object
     */
    FortunaTokenModel parseToken(final String token);
}
