package com.fortuna.accessControl.security.impl;


import com.fortuna.accessControl.security.IGQBToken;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaTokenModel;
import com.fortuna.core.utils.FortunaDateUtils;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

/**
 * Created by Anil on 5/14/18.
 */
@Component
public class GQBTokenImpl implements IGQBToken {

    public static final String SECRET = "gqb-secret-code";

    /**
     * Generates a JWT token containing username as subject, and userId and other as additional claims. These properties are taken from the specified
     * User object
     *
     * @param umUserEntity the user for which the token will be generated
     * @return the JWT token
     */
    @Override
    public String generateToken(UmUserEntity umUserEntity) {
        Claims claims = Jwts.claims().setSubject(umUserEntity.getUserName());
        claims.put("userId", umUserEntity.getId());
//        claims.put("companyId", umUserEntity.getUserCompanyId());
//        claims.put("branchId", umUserEntity.getUserBranchId());
        try {
            claims.put("roleName", umUserEntity.getUmUserRoleEntity().getUmRoleEntity().getName());
            claims.put("roleId", umUserEntity.getUmUserRoleEntity().getUmRoleEntity().getId());
            claims.put("roleLevel", umUserEntity.getUmUserRoleEntity().getUmRoleEntity().getLevel());
        } catch (Exception e) {
            e.printStackTrace();
            throw new FortunaException("Unauthorized access, cause: " + e.getCause() + ", message: " + e.getMessage());
        }
        String token = Jwts.builder().setClaims(claims)
                .setIssuedAt(FortunaDateUtils.localDateTimeIntoUtilDate(FortunaDateUtils.now()))
                .setExpiration(FortunaDateUtils.localDateTimeIntoUtilDate(FortunaDateUtils.addMinuteToDateTime(700)))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
        return token;
    }

    /**
     * Tries to parse specified String as a JWT token. If successful, returns FortunaTokenModel object
     * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
     *
     * @param token the JWT token to parse
     * @return the FortunaTokenModel object extracted from specified token or null if a token is invalid.
     */
    @Override
    public FortunaTokenModel parseToken(final String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
        FortunaTokenModel tokenInfoModel = new FortunaTokenModel();
        tokenInfoModel.setUserName(claims.getSubject());
        try {
            tokenInfoModel.setUserId(Long.parseLong(claims.get("userId").toString()));
//            tokenInfoModel.setBranchId(claims.get("branchId") != null ? Long.parseLong(claims.get("branchId").toString()) : null);
//            tokenInfoModel.setCompanyId(Long.parseLong(claims.get("companyId").toString()));
            tokenInfoModel.setRoleName((String) claims.get("roleName"));
            tokenInfoModel.setRoleId(Long.parseLong(claims.get("roleId").toString()));
            tokenInfoModel.setRoleLevel(Integer.parseInt(claims.get("roleLevel").toString()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new FortunaException("Unauthorized access, cause: " + e.getCause() + ", message: " + e.getMessage());
        }
        return tokenInfoModel;
    }
}
