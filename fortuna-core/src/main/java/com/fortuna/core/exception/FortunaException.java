package com.fortuna.core.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by anil on 7/12/17.
 */
@Getter
@Setter
public class FortunaException extends RuntimeException {
    private Object data;

    public FortunaException(String message) {
        this(message, null);
    }

    public FortunaException(String message, Object data) {
        super(message);
        this.data = data;
    }
}
