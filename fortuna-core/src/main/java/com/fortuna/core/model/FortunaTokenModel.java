package com.fortuna.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Anil on 5/14/18.
 */
@Getter
@Setter
public class FortunaTokenModel extends ModelBase {

    private Long companyId;

    private Long branchId;

    private Long userId;

    private String userName;

    private String roleName;

    private Long roleId;

    private Integer roleLevel;

    private Long formId;

    private Long applicationId;
}
