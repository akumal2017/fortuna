package com.fortuna.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by anil on 8/28/17.
 */
@Getter
@Setter
public class ReferencedTableModel extends ModelBase {
    String tableName;
    String columnName;
}
