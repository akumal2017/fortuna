package com.fortuna.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by anil on 5/10/18.
 */
@Getter
@Setter
public class FortunaDtoBase extends ModelBase {

    private Long id;

    private String name;

    private String recordState;

    private Long version;


}
