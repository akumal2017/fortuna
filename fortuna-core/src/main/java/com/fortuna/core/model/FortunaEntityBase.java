package com.fortuna.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by admin on 5/10/2018.
 */
@Getter
@Setter
@MappedSuperclass
public class FortunaEntityBase extends ModelBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;

    @Column(name = "created_by", updatable = false)
    private Long createdBy;

    @Column(name = "created_date", nullable = false)
    private Date createdDate;

    @Column(name = "last_modified_by")
    private Long lastModifiedBy;

    @Column(name = "last_modified_date")
    @JsonIgnore
    private Date lastModifiedDate;

    @Column(name = "company_id")
    private Long companyId;
    @Column(name = "branch_id")
    private Long branchId;

    @Column(name = "record_state")
    private String recordState;
}
