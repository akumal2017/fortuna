package com.fortuna.core.dto.mylunch.requestDto;

import com.fortuna.core.model.FortunaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOOrderRequestDto extends FortunaRequestDtoBase {
    List<OFOOrderDetailRequestDto> foodOrderDetailList;
    private String orderBy;
    private String contactNumber;
    private String deliveryAddress;
    private String deliveryType;
    private Double totalAmount;

}
