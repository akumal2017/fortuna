package com.fortuna.core.dto.accessControl.requestDto;

import com.fortuna.core.model.ModelBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/14/2018.
 */
@Getter
@Setter
public class UmUserLoginRequestDto extends ModelBase {
    private String userName;
    private String password;
}
