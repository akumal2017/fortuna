package com.fortuna.core.dto.mylunch.responseDto;

import com.fortuna.core.dto.mylunch.requestDto.OFOOrderDetailRequestDto;
import com.fortuna.core.model.FortunaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOOrderResponseDto extends FortunaResponseDtoBase {
    List<OFOOrderDetailRequestDto> foodOrderDetailList = new ArrayList<>();
    private String code;
    private String orderBy;
    private String contactNumber;
    private String deliveryAddress;
    private String deliveryType;
    private Double totalAmount;
    private String orderStatus;
    private Date orderDate;

}
