package com.fortuna.core.dto.mylunch.requestDto;

import com.fortuna.core.model.FortunaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOUpdateOrderRequestDto extends FortunaRequestDtoBase {
    private Long orderId;
    private String orderStatus;
}
