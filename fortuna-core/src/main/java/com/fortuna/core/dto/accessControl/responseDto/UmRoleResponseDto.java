package com.fortuna.core.dto.accessControl.responseDto;

import com.fortuna.core.model.FortunaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class UmRoleResponseDto extends FortunaResponseDtoBase {
    Integer level;
    String description;
}
