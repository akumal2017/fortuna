package com.fortuna.core.dto.mylunch.responseDto;

import com.fortuna.core.model.FortunaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOOrderCountResponseDto extends FortunaResponseDtoBase {
    private Long totalOrder;
    private Long totalSales;
    private Long totalDelivered;
    private Long totalPickWhenReady;
}
