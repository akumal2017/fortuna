package com.fortuna.core.dto.mylunch.responseDto;

import com.fortuna.core.model.FortunaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOFoodResponseDto extends FortunaResponseDtoBase {
    private String code;
    private String image;
    private Double price;
    private String description;
    private Integer quantity = 0;
}
