package com.fortuna.core.dto.mylunch.requestDto;

import com.fortuna.core.model.FortunaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOFoodRequestDto extends FortunaRequestDtoBase {
    private String code;
    private String image;
    private Double price;
    private String description;

}
