package com.fortuna.core.dto.accessControl.requestDto;

import com.fortuna.core.model.FortunaRequestDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class UmRoleRequestDto extends FortunaRequestDtoBase {
    Integer level;
    String description;
}
