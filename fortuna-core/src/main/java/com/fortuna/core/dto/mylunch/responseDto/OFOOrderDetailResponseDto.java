package com.fortuna.core.dto.mylunch.responseDto;

import com.fortuna.core.dto.mylunch.requestDto.OFOFoodRequestDto;
import com.fortuna.core.model.FortunaResponseDtoBase;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by admin on 5/13/2018.
 */
@Getter
@Setter
public class OFOOrderDetailResponseDto extends FortunaResponseDtoBase {
    private Integer quantity;
    private OFOFoodRequestDto ofoFoodEntity;

}
