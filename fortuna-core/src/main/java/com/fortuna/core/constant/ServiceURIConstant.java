package com.fortuna.core.constant;

/**
 * Created by admin on 5/13/2018.
 */
public final class ServiceURIConstant extends WebResourceConstant {
    public static final String ROOT_HOST = "http://localhost:8080";
    public static final String ROOT_GATEWAY_API = "/gymhouse";

}
