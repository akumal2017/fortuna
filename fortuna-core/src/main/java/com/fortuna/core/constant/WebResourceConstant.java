package com.fortuna.core.constant;

/**
 * Created by Anil on 5/10/18.
 */
public class WebResourceConstant {


    /**
     * Common api end points
     */
    public static final String BASE_API = "/api";
    public static final String CREATE = "/create";
    public static final String UPDATE = "/update";
    public static final String DELETE = "/{id}";
    public static final String GET_ALL = "/list";
    public static final String GET_ALL_WITH_PAGE = "/list/{currentPage}/{pageSize}";
    public static final String GET = "/{id}";

    //For file
    public static final String FILE = "/file";

    public static final String DISPLAY_FILE = "/display/{fileName}";
    public static final String FILE_DOWNLOAD = "/download";
    public static final String SEARCH = "/search/{currentPage}/{pageSize}";
    public static final String UPLOAD = "/upload";

    public static final String MOVE_DOCUMENT = "/move-file";


    public static final String LOGGER = "/logger/{status}";
    //header Constants
    public static final String FORM_HEADER = "form";
    public static final String APPLICATION_HEADER = "application";
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * Module wise interface for api end points
     */
    public interface UserManagement {
        String USER_MANAGEMENT = BASE_API + "/user-management";

        String UM_ROLE = USER_MANAGEMENT + "/role";

        String UM_USER = USER_MANAGEMENT + "/user";
        String UM_COMPANY = USER_MANAGEMENT + "/company";

        String UM_BRANCH = USER_MANAGEMENT + "/branch";

        String UM_AUTHENTICATE = "/auth";
        String CHANGE_PASSWORD = "/chhangepassword";


    }

    public interface MyLunch {
        String MY_LUNCH = BASE_API + "/mylunch";
        String FOOD = MY_LUNCH + "/food";
        String ORDER_FOOD = MY_LUNCH + "/orderfood";
        String UPDATE_ORDER_STATUS = "/orderstatus";
        String ORDER_DETAIL = MY_LUNCH + "/orderdetail";
        String ADMIN_DASHBOARD = MY_LUNCH + "/admin";
        String DASHBOARD_ORDER_COUNT = "/ordercount";

        String ORDER_ID = "/order";
        String CLOSE_SALES = "/closesales";

    }


}
