package com.fortuna.core.enums;

/**
 * Created by anil on 5/10/18.
 */
public enum RecordState {

    /**
     * A  = Active Record
     * X  = Deleted Record
     * W = Pending / Waiting to Save
     * E  = Approved Record in Modified State
     * R  = Rejected Record
     * T  = Rejected Record, which were modified after approved.
     * D  = Draft Record
     * U  = Sent for Re-approval, which were rejected before approval.
     * S  = Sent for Re-approval, which were rejected after modifying approved record.
     */

    ACTIVE("A", "Active Record"),
    DELETED("X", "Deleted Record"),
    WAITING("W", "Pending/Waiting to save"),
    MODIFIED("E", "Approved Record in Modified State"),
    REJECTED("R", "Rejected Record"),
    REJECTED_AFTER_EDIT("T", "Rejected Record, which were modified after approved"),
    DRAFT("D", "Draft Record"),
    REAPPROVAL_OF_NEW("U", "Sent for Re-approval, which were rejected before approval"),
    REAPPROVAL_OF_EDIT("S", "Sent for Re-approval, which were rejected after modifying approved record");

    private String status;
    private String description;

    RecordState(String status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getStatus() {
        return this.status;
    }

    public String getDescription() {
        return this.description;
    }
}
