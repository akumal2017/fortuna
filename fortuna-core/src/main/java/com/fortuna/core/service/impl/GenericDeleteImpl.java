package com.fortuna.core.service.impl;


import com.fortuna.core.enums.RecordState;
import com.fortuna.core.model.ReferencedTableModel;
import com.fortuna.core.service.IGenericDelete;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anil on 8/28/17.
 */
@Component
public class GenericDeleteImpl implements IGenericDelete<Long> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean isDependencyExist(String tableName, Long id) {
        /*TODO Fix Me: By getting db name dynamically */
        String dbName = "fortuna";
        List<ReferencedTableModel> referencedTableModelList = fetchReferencedTables(dbName, tableName);
        for (ReferencedTableModel referenceTable : referencedTableModelList) {
            if (count(referenceTable.getTableName(), referenceTable.getColumnName(), id) > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<ReferencedTableModel> fetchReferencedTables(String dbName, String tableName) {

        /**
         * Query Example:
         * SELECT
         * DISTINCT(TABLE_NAME),COLUMN_NAME
         * FROM
         * INFORMATION_SCHEMA.KEY_COLUMN_USAGE
         * WHERE
         * REFERENCED_TABLE_SCHEMA = 'nova_module' AND
         * REFERENCED_TABLE_NAME = 'system_setup_role';
         */
        List<Object[]> resultList = entityManager.createNativeQuery("SELECT DISTINCT(TABLE_NAME),COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE  where REFERENCED_TABLE_SCHEMA=:dbName AND REFERENCED_TABLE_NAME=:tableName")
                .setParameter("dbName", dbName)
                .setParameter("tableName", tableName)
                .getResultList();
        return rowMapper(resultList);

    }

    @Override
    public boolean deleteChildRecords(List<ReferencedTableModel> referencedTableModelList, Long aLong) {
        return false;
    }

    @Override
    public long count(String tableName, String columnName, Long value) {
        Number count = (Number) entityManager.createNativeQuery("SELECT COUNT(*) FROM " + tableName + " where status_flag !=:statusFlag and " + columnName + "=:columnValue")
                .setParameter("statusFlag", RecordState.DELETED.getStatus())
                .setParameter("columnValue", value)
                .getSingleResult();
        return count.longValue();
    }

    /**
     * Maps each tuple to {@link ReferencedTableModel}
     *
     * @param result
     * @return new object of ReferencedTableModel
     */
    private ReferencedTableModel rowMapper(Object[] result) {
        ReferencedTableModel referencedTableModel = new ReferencedTableModel();
        referencedTableModel.setTableName(result[0].toString());
        referencedTableModel.setColumnName(result[1].toString());
        return referencedTableModel;
    }

    /**
     * Maps result set to list of {@link ReferencedTableModel}
     *
     * @param resultList - result set of query
     * @return - list of {@link ReferencedTableModel}
     */
    private List<ReferencedTableModel> rowMapper(List<Object[]> resultList) {
        List<ReferencedTableModel> referencedTableModels = new ArrayList<>();
        for (Object[] result : resultList) {
            referencedTableModels.add(rowMapper(result));
        }
        return referencedTableModels;
    }
}
