package com.fortuna.core.service.impl;


import com.fortuna.core.enums.RecordState;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaEntityBase;
import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.core.service.ICrudService;
import com.fortuna.core.service.IGenericDelete;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

public class CrudServiceImpl<T, ID extends Serializable> implements ICrudService<T, ID> {

    protected ICrudRepository<T, ID> crudRepository;

    @Autowired
    private IGenericDelete genericDelete;

    public CrudServiceImpl(ICrudRepository<T, ID> crudRepository) {
        this.crudRepository = crudRepository;
    }


    @Override
    public <S extends T> S save(S entity) {
        return crudRepository.save(entity);
    }

    @Override
    public <S extends T> S update(S entity) {
        return crudRepository.update(entity);
    }

    @Override
    public T findOne(ID id) {
        return crudRepository.findOne(id);
    }

    @Override
    public boolean exists(ID id) {
        return crudRepository.exists(id);
    }

    @Override
    public List<T> findAll() {
        return crudRepository.findAll();
    }


    @Override
    public List<T> findAll(ID id) {
        return crudRepository.findAll(id);
    }

    @Override
    public Long count() {
        return crudRepository.count();
    }

    @Override
    public Long count(String... recordStates) {
        return crudRepository.count(recordStates);
    }

    @Override
    public boolean delete(ID id) {
        if (genericDelete.isDependencyExist(crudRepository.getTableName(), id)) {
            throw new FortunaException("Cannot delete the record with ID: " + id + ". It is dependent for one or more records.");
        }
        T entity = findOne(id);
        setEntityPropsOnDelete(entity);
        crudRepository.update(entity);
        return true;
    }


    @Override
    public boolean delete(T entity) {
        if (genericDelete.isDependencyExist(crudRepository.getTableName(), getId(entity))) {
            throw new FortunaException("Cannot delete the record with ID: " + getId(entity) + ". It has been used in other records.");
        }
        setEntityPropsOnDelete(entity);
        crudRepository.update(entity);
        return true;
    }

    private Long getId(T entity) {
        if (entity instanceof FortunaEntityBase) {
            return ((FortunaEntityBase) entity).getId();
        }
        return null;
    }


    @Override
    public Long count(String name) {
        return crudRepository.count(name);
    }

    protected void setEntityPropsOnDelete(T entity) {
        if (entity instanceof FortunaEntityBase) {
            ((FortunaEntityBase) entity).setRecordState(RecordState.DELETED.getStatus());
        }
    }


}
