package com.fortuna.core.utils;

import java.util.List;

/**
 * Created by anil on 6/4/17.
 */
public interface IFortunaBeanMapper<Entity, DTO> {
    Entity mapToEntity(DTO viewModel);

    DTO mapToDTO(Entity entity);

    List<Entity> mapToEntity(List<DTO> dtoList);

    List<DTO> mapToDTO(List<Entity> entityList);
}
