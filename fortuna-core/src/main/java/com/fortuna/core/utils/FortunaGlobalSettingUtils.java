package com.fortuna.core.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anil on 5/10/18.
 */
public class FortunaGlobalSettingUtils {

    public static final String MAX_LOGIN_ATTEMPT = "MAX_LOGIN_ATTEMPT";
    public static final String IMAGE_UPLOAD_LOCATION = "IMAGE_UPLOAD_LOCATION";
    public static final String ROOT_UPLOAD_LOCATION = "ROOT_UPLOAD_LOCATION";

    private static Map<String, String> globalSettingMap = new HashMap<>();

    public static void setGlobalSettingMap(Map<String, String> systemResource) {
        globalSettingMap = systemResource;
    }

    public static String getGlobalSettingByKey(String key) {
        return globalSettingMap.get(key);
    }
}
