package com.fortuna.core.utils;


import com.fortuna.core.model.FortunaTokenModel;
import org.springframework.stereotype.Component;

/**
 * Created by Anil on 5/10/18.
 */
@Component
public class FortunaTokenUtils {

    private static FortunaTokenModel fortunaTokenModel;

    public static FortunaTokenModel getGQBTokenModel() {
        return fortunaTokenModel;
    }

    public static void setGQBTokenModel(final FortunaTokenModel fortunaTokenModel) {
        FortunaTokenUtils.fortunaTokenModel = fortunaTokenModel;
    }
}
