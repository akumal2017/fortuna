package com.fortuna.core.controller;


import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.model.FortunaResponseObj;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 5/13/2018.
 */
public abstract class FortunaRestTemplateControllerBase<Dto> {
    protected String serviceURI;
    private RestTemplate restTemplate;

    public FortunaRestTemplateControllerBase(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostMapping(WebResourceConstant.CREATE)
    public ResponseEntity<FortunaResponseObj> create(@RequestBody @Valid Dto dto) {
//        return restTemplate.postForEntity(this.getServiceURI() +WebResourceConstant.CREATE, dto, FortunaResponseObj.class);
        HttpEntity<Dto> requestHttpEntity = new HttpEntity<>(dto);
        ResponseEntity<FortunaResponseObj> gqbResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.CREATE, HttpMethod.POST, requestHttpEntity, FortunaResponseObj.class);
        return gqbResponse;
    }

    @PutMapping(WebResourceConstant.UPDATE)
    public ResponseEntity<FortunaResponseObj> update(@RequestBody @Valid Dto dto) {
//       return restTemplate.postForEntity(this.getServiceURI()+WebResourceConstant.UPDATE,dto,FortunaResponseObj.class);
        HttpEntity<Dto> requestHttpEntity = new HttpEntity<>(dto);
        ResponseEntity<FortunaResponseObj> gqbResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.UPDATE, HttpMethod.PUT, requestHttpEntity, FortunaResponseObj.class);
        return gqbResponse;
    }

    @DeleteMapping(WebResourceConstant.DELETE)
    public ResponseEntity<FortunaResponseObj> delete(@PathVariable Long id) {
        Map<String, Long> pathVariableMap = new HashMap<>();
        pathVariableMap.put("id", id);
        ResponseEntity<FortunaResponseObj> gqbResponse = restTemplate.exchange(this.getServiceURI() + WebResourceConstant.DELETE, HttpMethod.DELETE, null, FortunaResponseObj.class, pathVariableMap);
        return gqbResponse;
    }

    @GetMapping(WebResourceConstant.GET)
    public ResponseEntity<FortunaResponseObj> get(@PathVariable Long id) {
        FortunaResponseObj gqbResponse = restTemplate.getForObject(this.getServiceURI() + WebResourceConstant.GET, FortunaResponseObj.class, id);
        return new ResponseEntity<>(gqbResponse, HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET_ALL)
    public ResponseEntity<FortunaResponseObj> getAll() {
        FortunaResponseObj gqbResponse = restTemplate.getForObject(this.getServiceURI() + WebResourceConstant.GET_ALL, FortunaResponseObj.class);
        return new ResponseEntity<>(gqbResponse, HttpStatus.OK);
    }

    protected abstract String getServiceURI();
}
