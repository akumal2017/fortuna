package com.fortuna.core.controller;

import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.core.service.ICrudService;
import com.fortuna.core.utils.IFortunaBeanMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Anil on 5/10/18.
 */

public abstract class FortunaControllerBase<Entity, Dto> {

    protected ICrudService iCrudService;
    protected IFortunaBeanMapper<Entity, Dto> igqReqbBeanMapper;
    protected IFortunaBeanMapper<Entity, Dto> igqResqbBeanMapper;


    public FortunaControllerBase(ICrudService iCrudService, IFortunaBeanMapper igqReqbBeanMapper, IFortunaBeanMapper igqResqbBeanMapper) {
        this.iCrudService = iCrudService;
        this.igqReqbBeanMapper = igqReqbBeanMapper;
        this.igqResqbBeanMapper = igqResqbBeanMapper;
    }

    @PostMapping(WebResourceConstant.CREATE)
    public ResponseEntity<FortunaResponseObj> create(@RequestBody @Valid Dto dto) {
        Entity entity = igqReqbBeanMapper.mapToEntity(dto);
        iCrudService.save(entity);
        // setCreateEntityProperties(entity);
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().message("Record has been created.").build(), HttpStatus.OK);
    }

    @PutMapping(WebResourceConstant.UPDATE)
    public ResponseEntity<FortunaResponseObj> update(@RequestBody @Valid Dto dto) {
        Entity entity = igqReqbBeanMapper.mapToEntity(dto);
        //  setUpdateEntityProperties(entity);
        iCrudService.update(entity);
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().message("Record has been updated.").build(), HttpStatus.OK);
    }

    @DeleteMapping(WebResourceConstant.DELETE)
    public ResponseEntity<FortunaResponseObj> delete(@PathVariable Long id) {
        iCrudService.delete(id);
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(id).message("Record with id: " + id + " deleted.").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET)
    public ResponseEntity<FortunaResponseObj> get(@PathVariable Long id) {
        Entity entity = (Entity) iCrudService.findOne(id);
        if (entity == null) {
            throw new FortunaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(igqResqbBeanMapper.mapToDTO(entity)).message("Success").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.GET_ALL)
    public ResponseEntity<FortunaResponseObj> getAll() {
        List<Entity> entities = iCrudService.findAll();
        if (entities.size() == 0) {
            throw new FortunaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(igqResqbBeanMapper.mapToDTO(entities)).message("Success").build(), HttpStatus.OK);
    }

    public ResponseEntity<FortunaResponseObj> getAll(Integer currentPage, Integer pageSize) {
        return null;
    }


}
