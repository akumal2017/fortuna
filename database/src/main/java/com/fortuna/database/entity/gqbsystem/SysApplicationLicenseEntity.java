package com.fortuna.database.entity.gqbsystem;

import com.fortuna.core.model.FortunaEntityBase;
import com.fortuna.database.entity.usermanagement.UmBranchEntity;
import com.fortuna.database.entity.usermanagement.UmCompanyEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by admin on 5/16/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "sys_application_license")
public class SysApplicationLicenseEntity extends FortunaEntityBase {
    @Column(name = "license_key")
    private String licenseKey;
    @Column(name = "is_active_license")
    private Boolean isActiveLicense;
    @Column(name = "valid_from_date")
    private Date validfromDate;
    @Column(name = "valid_to_date")
    private Date validToDate;

    @ManyToOne
    @JoinColumn(name = "application_id")
    private SysApplicationEntity sysApplicationEntity;

    @ManyToOne
    @JoinColumn(name = "client_company_id")
    private UmCompanyEntity umCompanyEntity;

    @ManyToOne
    @JoinColumn(name = "client_branch_id")
    private UmBranchEntity umBranchEntity;

    @Version
    @Column(name = "version")
    private Long version = 0L;


}
