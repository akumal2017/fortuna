package com.fortuna.database.entity.usermanagement;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Date;

/**
 * Created by Anil on 5/10/18.
 */

@Getter
@Setter
@Entity
@Table(name = "um_company")
public class UmCompanyEntity extends FortunaEntityBase {

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "hasBranch")
    private Boolean hasBranch;

    @Column(name = "company_address1")
    private String companyAddress1;

    @Column(name = "company_address2")
    private String companyAddress2;

    @Column(name = "telephone1")
    private String telephone1;

    @Column(name = "telephone2")
    private String telephone2;

    @Column(name = "fax")
    private String fax;

    @Column(name = "emal")
    private String email;

    @Column(name = "website")
    private String website;

    @Column(name = "registration_number")
    private String registrationNumber;

    @Column(name = "registration_valid_date")
    private Date registrationValidDate;

    @Column(name = "registration_person_name")
    private String registrationPersonName;

    @Column(name = "registration_filename")
    private String registrationFileName;

    @Column(name = "pan_number")
    private String panNumber;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "logo_filename1")
    private String logoFileName1;

    @Column(name = "logo_filename2")
    private String logoFileName2;

    @Version
    @Column(name = "version")
    private Long version = 0L;

}
