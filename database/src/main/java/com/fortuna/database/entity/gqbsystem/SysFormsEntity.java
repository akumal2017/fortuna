package com.fortuna.database.entity.gqbsystem;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Anil on 5/16/18.
 */
@Getter
@Setter
@Entity
@Table(name = "sys_forms")
public class SysFormsEntity extends FortunaEntityBase {

    @Column(name = "code", unique = true)
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    private SysApplicationEntity sysApplicationEntity;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "level")
    private Integer level;

    @Column(name = "form_type")
    private String formType;

    @Column(name = "icon")
    private String icon;

    @Version
    @Column(name = "version")
    private Long version = 0L;

    @Transient
    public SysFormsEntity getSysFormsEntity(Long id) {
        this.setId(id);
        return this;
    }
}
