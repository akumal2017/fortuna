package com.fortuna.database.entity.mylunch;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by admin on 5/23/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "ofo_sequence")
public class OFOSequenceEntity extends FortunaEntityBase {
    @Column(name = "sequence_value")
    private Long sequenceValue;
    @Column(name = "prefix")
    private String prefix;
    @Column(name = "suffix")
    private String suffix;
    @Version
    @Column(name = "version")
    private Long version = 0L;
}
