package com.fortuna.database.entity.usermanagement;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Anil on 5/10/18.
 */
@Getter
@Setter
@Entity
@Table(name = "um_user")
public class UmUserEntity extends FortunaEntityBase {

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "image_file")
    private String imageFile;

    @Column(name = "address_1")
    private String addressOne;

    @Column(name = "address_2")
    private String addressTwo;

    @Column(name = "contact_1")
    private String contactOne;

    @Column(name = "contact_2")
    private String contactTwo;

    @Column(name = "email")
    private String email;

    @Column(name = "super_user")
    private Boolean superUser = false;

    @Column(name = "is_suspended")
    private Boolean isSuspended = false;

    @Column(name = "login_attempt")
    private Integer loginAttempt = 0;

    @Transient
    private boolean passwordMatch = false;

    @OneToOne(mappedBy = "umUserEntity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private UmUserRoleEntity umUserRoleEntity;

    @Version
    @Column(name = "version")
    private Long version = 0L;

    @Column(name = "user_company_id")
    private Long userCompanyId;

    @Column(name = "user_branch_id")
    private Long userBranchId;
}
