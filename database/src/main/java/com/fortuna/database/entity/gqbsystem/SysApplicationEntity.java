package com.fortuna.database.entity.gqbsystem;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by pradip on 7/4/17.
 */
@Getter
@Setter
@Entity
@Table(name = "sys_application")
public class SysApplicationEntity extends FortunaEntityBase {

    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "icon")
    private String icon;

    @Column(name = "url")
    private String url;

    @Version
    @Column(name = "version")
    private Long version = 0L;

    @Transient
    public SysApplicationEntity getSysApplicationEntity(Long id) {
        this.setId(id);
        return this;
    }
}

