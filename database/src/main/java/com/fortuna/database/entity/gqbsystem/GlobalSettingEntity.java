package com.fortuna.database.entity.gqbsystem;


import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * Created by admin on 5/14/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "global_setting")
public class GlobalSettingEntity extends FortunaEntityBase {
    @Column(name = "setting_value")
    private String settingValue;

    @Version
    @Column(name = "version")
    private Long version = 0L;
}
