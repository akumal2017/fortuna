package com.fortuna.database.entity.usermanagement;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by anil on 5/10/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "um_role")
public class UmRoleEntity extends FortunaEntityBase {

    @Column(name = "level")
    private Integer level;

    @Column(name = "description")
    private String description;

    @Version
    @Column(name = "version")
    private Long version = 0L;

    @Transient
    public UmRoleEntity getSystemRoleEntity(Long id) {
        this.setId(id);
        return this;
    }
}
