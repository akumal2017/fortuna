package com.fortuna.database.entity.gqbsystem;

import com.fortuna.core.model.FortunaEntityBase;
import com.fortuna.database.entity.usermanagement.UmRoleEntity;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Anil on 5/16/18.
 */
@Getter
@Setter
@Entity
@Table(name = "sys_config_custom_privilege")
public class SysConfigCustomPrivilegeEntity extends FortunaEntityBase {
    /**
     * foreign keys
     */
    @ManyToOne
    @JoinColumn(name = "application_id")
    private SysApplicationEntity sysApplicationEntity;


    @ManyToOne
    @JoinColumn(name = "form_id")
    private SysFormsEntity sysFormsEntity;

    @Column(name = "apply_to_role")
    private Boolean applyToRole = true;

    @Column(name = "apply_to_user")
    private Boolean applyToUser = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private UmRoleEntity umRoleEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UmUserEntity umUserEntity;

    // table specific field

    @Column(name = "custom_privilege", columnDefinition = "Text")
    private String privilege;

    @Version
    @Column(name = "version")
    private Long version = 0L;
}
