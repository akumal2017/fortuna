package com.fortuna.database.entity.mylunch;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "ofo_food_order")
public class OFOFoodOrderEntity extends FortunaEntityBase {
    @Column(name = "code")
    private String code;
    @Column(name = "order_by")
    private String orderBy;
    @Column(name = "delivery_address")
    private String deliveryAddress;
    @Column(name = "contact_number")
    private String contactNumber;
    @Column(name = "order_date")
    private Date orderDate = new Date();
    @Column(name = "delivery_type")
    private String deliveryType;
    @Column(name = "order_status")
    private String orderStatus;
    @Column(name = "total_amount")
    private Double totalAmount;
    @Version
    @Column(name = "version")
    private Long version;
    @OneToMany(mappedBy = "ofoFoodOrderEntity", fetch = FetchType.LAZY)
    private List<OFOFoodOrderDetailEntity> foodOrderDetailList;
}
