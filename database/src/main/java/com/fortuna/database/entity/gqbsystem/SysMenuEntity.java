package com.fortuna.database.entity.gqbsystem;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by  Anil on 5/16/18.
 */
@Getter
@Setter
@Entity
@Table(name = "sys_menu")
public class SysMenuEntity extends FortunaEntityBase {

    /**
     * foreign keys
     */
    @ManyToOne
    @JoinColumn(name = "sys_sm_id")
    private SysApplicationEntity applicationEntity;

    @ManyToOne
    @JoinColumn(name = "form_id")
    private SysFormsEntity sysFormsEntity;

    @Column(name = "path")
    private String path;

    @Column(name = "has_child")
    private Boolean hasChild = false;

    @Column(name = "total_child")
    private Integer totalChild = 0;

    @Column(name = "own_code")
    private String ownCode;

    @Column(name = "parent_code")
    private String parentCode;

    @Version
    @Column(name = "version")
    private Long version = 0L;

}
