package com.fortuna.database.entity.mylunch;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by admin on 5/23/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "ofo_food_order_detial_archive")
public class OFOFoodOrderDetailArchiveEntity extends FortunaEntityBase {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_id")
    private OFOFoodEntity ofoFoodEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_order_id")
    private OFOFoodOrderArchiveEntity ofoFoodOrderEntity;
    @Column(name = "quantity")
    private Integer quantity;

}
