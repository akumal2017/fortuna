package com.fortuna.database.entity.usermanagement;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Anil on 7/5/17.
 * Email: bibek@drac.com.np
 */
@Getter
@Setter
@Entity
@Table(name = "um_branch")
public class UmBranchEntity extends FortunaEntityBase {

    @Column(name = "logoFileName")
    private String logoFileName;

    @Column(name = "type")
    private String type;

    @Column(name = "address1")
    private String address1;

    @Column(name = "address2")
    private String address2;

    @Column(name = "telephone1")
    private String telephone1;

    @Column(name = "telephone2")
    private String telephone2;

    @Column(name = "fax")
    private String fax;

    @Column(name = "email")
    private String email;

    @Column(name = "website")
    private String website;

    @Column(name = "registration_number")
    private String registrationNumber;

    @Column(name = "registration_person_name")
    private String registrationPersonName;

    @Column(name = "pan_number")
    private String panNumber;

    @Column(name = "vat_number")
    private String vatNumber;

    @ManyToOne
    @JoinColumn(name = "sys_company_id")
    private UmCompanyEntity umCompanyEntity;

    @Version
    @Column(name = "version")
    private Long version = 0L;
}
