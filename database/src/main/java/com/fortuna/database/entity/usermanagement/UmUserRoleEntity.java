package com.fortuna.database.entity.usermanagement;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * Created by Anil on 5/10/18.
 */

@Getter
@Setter
@Entity
@Table(name = "um_user_role")
public class UmUserRoleEntity extends FortunaEntityBase {

    //foreign keys
    @OneToOne
    @JoinColumn(name = "user_id")
    private UmUserEntity umUserEntity;

    @OneToOne
    @JoinColumn(name = "role_id")
    private UmRoleEntity umRoleEntity;

    //table speciific fields

    @Column(name = "time_limit")
    private Boolean timeLimit = false;

    @Column(name = "to_time")
    private Time toTime;

    @Column(name = "from_time")
    private Time fromTime;

    @Column(name = "date_limit")
    private Boolean dateLimit = false;

    @Column(name = "from_date")
    @Temporal(TemporalType.DATE)
    private Date fromDate;

    @Column(name = "to_date")
    @Temporal(TemporalType.DATE)
    private Date toDate;
}
