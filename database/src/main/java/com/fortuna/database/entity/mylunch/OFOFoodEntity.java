package com.fortuna.database.entity.mylunch;

import com.fortuna.core.model.FortunaEntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
@Getter
@Setter
@Entity
@Table(name = "ofo_food")
public class OFOFoodEntity extends FortunaEntityBase {
    @Column(name = "code")
    private String code;
    @Column(name = "image")
    private String image;
    @Column(name = "price")
    private Double price;
    @Column(name = "description")
    private String description;
    @Version
    @Column(name = "version")
    private Long version = 0L;
    @OneToMany(mappedBy = "ofoFoodEntity", fetch = FetchType.LAZY)
    private List<OFOFoodOrderDetailEntity> foodOrderDetailList;
}
