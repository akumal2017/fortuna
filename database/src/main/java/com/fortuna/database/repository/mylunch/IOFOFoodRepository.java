package com.fortuna.database.repository.mylunch;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.mylunch.OFOFoodEntity;

/**
 * Created by admin on 5/10/2018.
 */
public interface IOFOFoodRepository extends ICrudRepository<OFOFoodEntity, Long> {
}
