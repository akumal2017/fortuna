package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderArchiveEntity;
import com.fortuna.database.entity.mylunch.QOFOFoodOrderArchiveEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderArchiveRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOFoodOrderArchiveRepositoryImpl extends CrudRepositoryImpl<OFOFoodOrderArchiveEntity, Long> implements IOFOFoodOrderArchiveRepository {
    public OFOFoodOrderArchiveRepositoryImpl() {
        super(OFOFoodOrderArchiveEntity.class);
    }

    @Override
    public Long updateOrderStatus(Long orderCode, String orderStatus) {

        QOFOFoodOrderArchiveEntity qofoFoodOrderArchiveEntity = QOFOFoodOrderArchiveEntity.oFOFoodOrderArchiveEntity;

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(entityManager, qofoFoodOrderArchiveEntity);
        return jpaUpdateClause
                .where(qofoFoodOrderArchiveEntity.id.eq(orderCode))
                .set(qofoFoodOrderArchiveEntity.orderStatus, orderStatus)
                .execute();


    }

    @Override
    public Long countByDeliveryType(String deliveryType) {
        QOFOFoodOrderArchiveEntity qofoFoodOrderArchiveEntity = QOFOFoodOrderArchiveEntity.oFOFoodOrderArchiveEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderArchiveEntity)
                .where(qofoFoodOrderArchiveEntity.deliveryType.toLowerCase().eq(deliveryType.toLowerCase()),
                        qofoFoodOrderArchiveEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetchCount();

    }

    @Override
    public Long countByOrderStatus(String orderStatus) {
        QOFOFoodOrderArchiveEntity qofoFoodOrderArchiveEntity = QOFOFoodOrderArchiveEntity.oFOFoodOrderArchiveEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderArchiveEntity)
                .where(qofoFoodOrderArchiveEntity.orderStatus.toLowerCase().eq(orderStatus.toLowerCase()),
                        qofoFoodOrderArchiveEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetchCount();
    }
}
