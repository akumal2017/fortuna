package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailArchiveEntity;
import com.fortuna.database.entity.mylunch.QOFOFoodOrderDetailArchiveEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderDetailArchiveRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOFoodOrderDetailArchiveRepositoryImpl extends CrudRepositoryImpl<OFOFoodOrderDetailArchiveEntity, Long> implements IOFOFoodOrderDetailArchiveRepository {
    public OFOFoodOrderDetailArchiveRepositoryImpl() {
        super(OFOFoodOrderDetailArchiveEntity.class);
    }

    @Override
    public List<OFOFoodOrderDetailArchiveEntity> getListByOrderId(Long orderId) {
        QOFOFoodOrderDetailArchiveEntity qofoFoodOrderDetailArchiveEntity = QOFOFoodOrderDetailArchiveEntity.oFOFoodOrderDetailArchiveEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderDetailArchiveEntity)
                .where(qofoFoodOrderDetailArchiveEntity.ofoFoodOrderEntity.id.eq(orderId),
                        qofoFoodOrderDetailArchiveEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetch();
    }
}
