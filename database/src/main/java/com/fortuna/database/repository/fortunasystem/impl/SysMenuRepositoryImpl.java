package com.fortuna.database.repository.fortunasystem.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.SysMenuEntity;
import com.fortuna.database.repository.fortunasystem.ISysMenuRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class SysMenuRepositoryImpl extends CrudRepositoryImpl<SysMenuEntity, Long> implements ISysMenuRepository {
    public SysMenuRepositoryImpl() {
        super(SysMenuEntity.class);
    }
}
