package com.fortuna.database.repository.fortunasystem;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.gqbsystem.SysMenuEntity;

/**
 * Created by admin on 5/14/2018.
 */
public interface ISysMenuRepository extends ICrudRepository<SysMenuEntity, Long> {
}
