package com.fortuna.database.repository.fortunasystem.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.SysConfigCustomPrivilegeEntity;
import com.fortuna.database.repository.fortunasystem.ISysConfigCustomPrivilegeRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class SysConfgCustomPrivilegeRepositoryImpl extends CrudRepositoryImpl<SysConfigCustomPrivilegeEntity, Long> implements ISysConfigCustomPrivilegeRepository {
    public SysConfgCustomPrivilegeRepositoryImpl() {
        super(SysConfigCustomPrivilegeEntity.class);
    }
}
