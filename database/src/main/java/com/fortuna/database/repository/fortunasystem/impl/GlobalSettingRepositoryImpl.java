package com.fortuna.database.repository.fortunasystem.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.GlobalSettingEntity;
import com.fortuna.database.repository.fortunasystem.IGlobalSettingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class GlobalSettingRepositoryImpl extends CrudRepositoryImpl<GlobalSettingEntity, Long> implements IGlobalSettingRepository {
    public GlobalSettingRepositoryImpl() {
        super(GlobalSettingEntity.class);
    }
}
