package com.fortuna.database.repository.fortunasystem;


import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.gqbsystem.SysConfigCustomPrivilegeEntity;

/**
 * Created by admin on 5/14/2018.
 */
public interface ISysConfigCustomPrivilegeRepository extends ICrudRepository<SysConfigCustomPrivilegeEntity, Long> {
}
