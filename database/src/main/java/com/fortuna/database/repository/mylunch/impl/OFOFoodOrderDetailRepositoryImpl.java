package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailEntity;
import com.fortuna.database.entity.mylunch.QOFOFoodOrderDetailEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderDetailRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOFoodOrderDetailRepositoryImpl extends CrudRepositoryImpl<OFOFoodOrderDetailEntity, Long> implements IOFOFoodOrderDetailRepository {
    public OFOFoodOrderDetailRepositoryImpl() {
        super(OFOFoodOrderDetailEntity.class);
    }

    @Override
    public List<OFOFoodOrderDetailEntity> getListByOrderId(Long orderId) {
        QOFOFoodOrderDetailEntity qofoFoodOrderDetailEntity = QOFOFoodOrderDetailEntity.oFOFoodOrderDetailEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderDetailEntity)
                .where(qofoFoodOrderDetailEntity.ofoFoodOrderEntity.id.eq(orderId),
                        qofoFoodOrderDetailEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetch();
    }

    @Override
    public Integer clearTableData() {
        return entityManager.createQuery("DELETE FROM " + this.tableName).executeUpdate();
    }
}
