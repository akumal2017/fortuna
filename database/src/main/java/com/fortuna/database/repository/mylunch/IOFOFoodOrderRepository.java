package com.fortuna.database.repository.mylunch;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.mylunch.OFOFoodOrderEntity;

/**
 * Created by admin on 5/10/2018.
 */
public interface IOFOFoodOrderRepository extends ICrudRepository<OFOFoodOrderEntity, Long> {
    Long updateOrderStatus(Long orderCode, String orderStatus);

    Long countByDeliveryType(String deliveryType);

    Long countByOrderStatus(String orderStatus);

    Integer clearTableData();

}
