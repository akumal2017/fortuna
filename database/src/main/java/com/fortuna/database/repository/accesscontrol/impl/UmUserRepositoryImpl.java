package com.fortuna.database.repository.accesscontrol.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.usermanagement.QUmUserEntity;
import com.fortuna.database.entity.usermanagement.UmUserEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class UmUserRepositoryImpl extends CrudRepositoryImpl<UmUserEntity, Long> implements IUmUserRepository {
    //    private static QUmUserEntity qUmUserEntity;
//    private static JPAQueryFactory jpaQueryFactory;
    public UmUserRepositoryImpl() {
        super(UmUserEntity.class);
//        qUmUserEntity = QUmUserEntity.umUserEntity;
//        jpaQueryFactory = new JPAQueryFactory(entityManager);
    }

    @Override
    public UmUserEntity findByUserName(String userName) {
        QUmUserEntity qUmUserEntity = QUmUserEntity.umUserEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        UmUserEntity umUserEntity = jpaQueryFactory
                .selectFrom(qUmUserEntity)
                .where(qUmUserEntity.userName.toLowerCase().eq(userName.toLowerCase())).fetchOne();
        return umUserEntity;
    }
}
