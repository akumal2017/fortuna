package com.fortuna.database.repository.accesscontrol.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.usermanagement.UmUserRoleEntity;
import com.fortuna.database.repository.accesscontrol.IUmUserRoleRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class UmUserRoleRepositoryImpl extends CrudRepositoryImpl<UmUserRoleEntity, Long> implements IUmUserRoleRepository {
    public UmUserRoleRepositoryImpl() {
        super(UmUserRoleEntity.class);
    }
}
