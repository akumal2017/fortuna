package com.fortuna.database.repository.mylunch;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailEntity;

import java.util.List;

/**
 * Created by admin on 5/10/2018.
 */
public interface IOFOFoodOrderDetailRepository extends ICrudRepository<OFOFoodOrderDetailEntity, Long> {
    List<OFOFoodOrderDetailEntity> getListByOrderId(Long orderId);

    Integer clearTableData();
}
