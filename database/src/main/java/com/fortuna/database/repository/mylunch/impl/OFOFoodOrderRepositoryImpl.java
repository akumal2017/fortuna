package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderEntity;
import com.fortuna.database.entity.mylunch.QOFOFoodOrderEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOFoodOrderRepositoryImpl extends CrudRepositoryImpl<OFOFoodOrderEntity, Long> implements IOFOFoodOrderRepository {
    public OFOFoodOrderRepositoryImpl() {
        super(OFOFoodOrderEntity.class);
    }

    @Override
    public Long updateOrderStatus(Long orderCode, String orderStatus) {

        QOFOFoodOrderEntity qofoFoodOrderEntity = QOFOFoodOrderEntity.oFOFoodOrderEntity;

        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(entityManager, qofoFoodOrderEntity);
        return jpaUpdateClause
                .where(qofoFoodOrderEntity.id.eq(orderCode))
                .set(qofoFoodOrderEntity.orderStatus, orderStatus)
                .execute();


    }

    @Override
    public Long countByDeliveryType(String deliveryType) {
        QOFOFoodOrderEntity qofoFoodOrderEntity = QOFOFoodOrderEntity.oFOFoodOrderEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderEntity)
                .where(qofoFoodOrderEntity.deliveryType.toLowerCase().eq(deliveryType.toLowerCase()),
                        qofoFoodOrderEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetchCount();

    }

    @Override
    public Long countByOrderStatus(String orderStatus) {
        QOFOFoodOrderEntity qofoFoodOrderEntity = QOFOFoodOrderEntity.oFOFoodOrderEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        return jpaQueryFactory.selectFrom(qofoFoodOrderEntity)
                .where(qofoFoodOrderEntity.orderStatus.toLowerCase().eq(orderStatus.toLowerCase()),
                        qofoFoodOrderEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetchCount();
    }

    @Override
    public Integer clearTableData() {
        return entityManager.createQuery("DELETE FROM " + this.tableName).executeUpdate();
    }
}
