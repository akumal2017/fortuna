package com.fortuna.database.repository.mylunch;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailArchiveEntity;

import java.util.List;

/**
 * Created by admin on 5/10/2018.
 */
public interface IOFOFoodOrderDetailArchiveRepository extends ICrudRepository<OFOFoodOrderDetailArchiveEntity, Long> {
    List<OFOFoodOrderDetailArchiveEntity> getListByOrderId(Long orderId);
}
