package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOFoodEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOFoodRepositoryImpl extends CrudRepositoryImpl<OFOFoodEntity, Long> implements IOFOFoodRepository {
    public OFOFoodRepositoryImpl() {
        super(OFOFoodEntity.class);
    }


}
