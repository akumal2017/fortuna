package com.fortuna.database.repository.accesscontrol.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.usermanagement.UmRoleEntity;
import com.fortuna.database.repository.accesscontrol.IUmRoleRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class UmRoleRepositoryImpl extends CrudRepositoryImpl<UmRoleEntity, Long> implements IUmRoleRepository {
    public UmRoleRepositoryImpl() {
        super(UmRoleEntity.class);
    }
}
