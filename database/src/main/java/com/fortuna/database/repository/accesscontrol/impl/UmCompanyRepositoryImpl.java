package com.fortuna.database.repository.accesscontrol.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.usermanagement.UmCompanyEntity;
import com.fortuna.database.repository.accesscontrol.IUmCompanyRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class UmCompanyRepositoryImpl extends CrudRepositoryImpl<UmCompanyEntity, Long> implements IUmCompanyRepository {
    public UmCompanyRepositoryImpl() {
        super(UmCompanyEntity.class);
    }
}
