package com.fortuna.database.repository.fortunasystem.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.SysApplicationEntity;
import com.fortuna.database.repository.fortunasystem.ISysApplicationRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class SysApplicationRepositoryImpl extends CrudRepositoryImpl<SysApplicationEntity, Long> implements ISysApplicationRepository {
    public SysApplicationRepositoryImpl() {
        super(SysApplicationEntity.class);
    }
}
