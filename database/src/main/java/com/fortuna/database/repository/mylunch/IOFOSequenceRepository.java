package com.fortuna.database.repository.mylunch;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.mylunch.OFOSequenceEntity;

/**
 * Created by admin on 5/10/2018.
 */
public interface IOFOSequenceRepository extends ICrudRepository<OFOSequenceEntity, Long> {
    OFOSequenceEntity getByName(String name);
}
