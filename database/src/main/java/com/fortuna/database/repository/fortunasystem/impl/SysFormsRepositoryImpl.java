package com.fortuna.database.repository.fortunasystem.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.SysFormsEntity;
import com.fortuna.database.repository.fortunasystem.ISysFormsRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class SysFormsRepositoryImpl extends CrudRepositoryImpl<SysFormsEntity, Long> implements ISysFormsRepository {
    public SysFormsRepositoryImpl() {
        super(SysFormsEntity.class);
    }
}
