package com.fortuna.database.repository.accesscontrol.impl;

import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.usermanagement.UmBranchEntity;
import com.fortuna.database.repository.accesscontrol.IUmBranchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class UmBranchRepositoryImpl extends CrudRepositoryImpl<UmBranchEntity, Long> implements IUmBranchRepository {
    public UmBranchRepositoryImpl() {
        super(UmBranchEntity.class);
    }
}
