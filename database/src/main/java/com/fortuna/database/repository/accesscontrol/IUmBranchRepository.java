package com.fortuna.database.repository.accesscontrol;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.usermanagement.UmBranchEntity;

/**
 * Created by admin on 5/10/2018.
 */
public interface IUmBranchRepository extends ICrudRepository<UmBranchEntity, Long> {
}
