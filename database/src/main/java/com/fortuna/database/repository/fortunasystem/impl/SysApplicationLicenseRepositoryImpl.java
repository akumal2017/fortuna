package com.fortuna.database.repository.fortunasystem.impl;


import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.gqbsystem.SysApplicationLicenseEntity;
import com.fortuna.database.repository.fortunasystem.ISysApplicationLicenseRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/14/2018.
 */
@Repository
public class SysApplicationLicenseRepositoryImpl extends CrudRepositoryImpl<SysApplicationLicenseEntity, Long> implements ISysApplicationLicenseRepository {
    public SysApplicationLicenseRepositoryImpl() {
        super(SysApplicationLicenseEntity.class);
    }
}
