package com.fortuna.database.repository.mylunch.impl;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.repository.impl.CrudRepositoryImpl;
import com.fortuna.database.entity.mylunch.OFOSequenceEntity;
import com.fortuna.database.entity.mylunch.QOFOSequenceEntity;
import com.fortuna.database.repository.mylunch.IOFOSequenceRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 5/10/2018.
 */
@Repository
public class OFOSequenceRepositoryImpl extends CrudRepositoryImpl<OFOSequenceEntity, Long> implements IOFOSequenceRepository {
    public OFOSequenceRepositoryImpl() {
        super(OFOSequenceEntity.class);
    }


    @Override
    public OFOSequenceEntity getByName(String name) {
        QOFOSequenceEntity qofoSequenceEntity = QOFOSequenceEntity.oFOSequenceEntity;
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(entityManager);
        OFOSequenceEntity ofoSequenceEntity = jpaQueryFactory
                .selectFrom(qofoSequenceEntity)
                .where(qofoSequenceEntity.name.toLowerCase().eq(name.toLowerCase()), qofoSequenceEntity.recordState.eq(RecordState.ACTIVE.getStatus()))
                .fetchOne();
        return ofoSequenceEntity;
    }
}
