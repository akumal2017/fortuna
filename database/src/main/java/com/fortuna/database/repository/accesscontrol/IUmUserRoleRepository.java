package com.fortuna.database.repository.accesscontrol;

import com.fortuna.core.repository.ICrudRepository;
import com.fortuna.database.entity.usermanagement.UmUserRoleEntity;

/**
 * Created by admin on 5/10/2018.
 */
public interface IUmUserRoleRepository extends ICrudRepository<UmUserRoleEntity, Long> {
}
