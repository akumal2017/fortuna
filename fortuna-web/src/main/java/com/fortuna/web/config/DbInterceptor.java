package com.fortuna.web.config;

import com.fortuna.core.enums.RecordState;
import com.fortuna.core.model.FortunaEntityBase;
import com.fortuna.core.utils.StringUtils;
import org.apache.commons.lang.ArrayUtils;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by anil on 8/18/17.
 */
public class DbInterceptor extends EmptyInterceptor {
    private static String RECORD_STATE = "recordState";
    private static String CREATED_DATE = "createdDate";
    private static String CREATED_BY = "createdBy";
    private static String LAST_MODIFIED_BY = "lastModifiedBy";
    private static String LAST_MODIFIED_DATE = "lastModifiedDate";
    private static String COMPANY_ID = "companyId";
    private static String BRANCH_ID = "branchId";

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        System.out.println("Db Interceptor - On Save = " + entity.toString());
        if (isInstanceOfBaseEntity(entity)) {
            FortunaEntityBase fortunaEntityBase = (FortunaEntityBase) entity;
//            TokenInfoModel tokenInfoModel = TokenInfoUtil.getTokenInfo();
            //Set common properties
            putValue(state, propertyNames, RECORD_STATE, StringUtils.isNull(fortunaEntityBase.getRecordState()) ? RecordState.ACTIVE.getStatus() : fortunaEntityBase.getRecordState(), entity);
            putValue(state, propertyNames, CREATED_DATE, new Date(), entity);
            putValue(state, propertyNames, CREATED_BY, 1L, entity);
            putValue(state, propertyNames, COMPANY_ID, 1L, entity);
//            putValue(state, propertyNames, BRANCH_ID, null, entity);
        }
        return true;
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        System.out.println("Db Interceptor - On Update = " + entity.toString());
        if (isInstanceOfBaseEntity(entity)) {
            FortunaEntityBase fortunaEntityBase = (FortunaEntityBase) entity;
//            TokenInfoModel tokenInfoModel = TokenInfoUtil.getTokenInfo();
            //Get old values
            Object createdBy = getValue(previousState, propertyNames, CREATED_BY, entity);
            Object createdDate = getValue(previousState, propertyNames, CREATED_DATE, entity);
            //set common properties
            putValue(currentState, propertyNames, CREATED_BY, createdBy, entity);
            putValue(currentState, propertyNames, CREATED_DATE, createdDate, entity);
            putValue(currentState, propertyNames, RECORD_STATE, StringUtils.isNull(fortunaEntityBase.getRecordState()) ? RecordState.ACTIVE.getStatus() : fortunaEntityBase.getRecordState(), entity);
            putValue(currentState, propertyNames, COMPANY_ID, 1L, entity);
            putValue(currentState, propertyNames, LAST_MODIFIED_BY, 1L, entity);
            putValue(currentState, propertyNames, LAST_MODIFIED_DATE, new Date(), entity);
//
        }

        return true;
    }


//    /**
//     * Checks if given entity is inherited from {@link AccountBaseEntity}
//     *
//     * @param entity
//     * @return true, if the given entity is sub-class of {@link AccountBaseEntity}, else false.
//     */
//    protected boolean isInstanceOfAccountBaseEntity(Object entity) {
//        return (entity instanceof AccountBaseEntity);
//    }

    /**
     * Checks if given entity is inherited from {@link FortunaEntityBase}
     *
     * @param entity
     * @return true, if the given entity is sub-class of {@link FortunaEntityBase}, else false.
     */
    protected boolean isInstanceOfBaseEntity(Object entity) {
        return (entity instanceof FortunaEntityBase);
    }

    /**
     * Sets value in entity
     *
     * @param currentState
     * @param propertyNames
     * @param propertyToSet
     * @param value
     * @param entity
     */
    protected void putValue(Object[] currentState, String[] propertyNames, String propertyToSet, Object value, Object entity) {
        int index = ArrayUtils.indexOf(propertyNames, propertyToSet);
        if (index >= 0) {
            currentState[index] = value;
        } else {
            System.err.println("Field '" + propertyToSet + "' not found on entity '" + entity.getClass().getName() + "'.");
        }
    }

    /**
     * Gets column value from entity
     *
     * @param state
     * @param propertyNames
     * @param propertyToGet
     * @param entity
     * @return column value, else null
     */
    protected Object getValue(Object[] state, String[] propertyNames, String propertyToGet, Object entity) {
        int index = ArrayUtils.indexOf(propertyNames, propertyToGet);
        if (index >= 0) {
            return state[index];
        } else {
            System.err.println("Field '" + propertyToGet + "' not found on entity '" + entity.getClass().getName() + "'.");
        }
        return null;
    }
}
