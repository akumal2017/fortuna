package com.fortuna.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created by anil on 6/2/17.
 */
@SpringBootApplication
@EntityScan({"com.fortuna"})
@ComponentScan({"com.fortuna"})
@EnableAspectJAutoProxy
public class FortunaWebStarter {
    public static void main(String[] args) {
        SpringApplication.run(FortunaWebStarter.class, args);
    }


}



