package com.fortuna.mylunch.controller;


import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.controller.FortunaControllerBase;
import com.fortuna.core.dto.mylunch.requestDto.OFOOrderRequestDto;
import com.fortuna.core.dto.mylunch.requestDto.OFOUpdateOrderRequestDto;
import com.fortuna.core.dto.mylunch.responseDto.OFOOrderResponseDto;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.core.utils.impl.FortunaBeanMapperImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderEntity;
import com.fortuna.mylunch.service.IOFOFoodOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Anil on 5/13/2018.
 */
@RestController
@RequestMapping(OFOOrderController.BASE_URL)
public class OFOOrderController extends FortunaControllerBase {
    public static final String BASE_URL = WebResourceConstant.MyLunch.ORDER_FOOD;

    private IOFOFoodOrderService foodOrderService;

    public OFOOrderController(IOFOFoodOrderService foodOrderService) {
        super(foodOrderService, new FortunaBeanMapperImpl(OFOFoodOrderEntity.class, OFOOrderRequestDto.class), new FortunaBeanMapperImpl(OFOFoodOrderEntity.class, OFOOrderResponseDto.class));
        this.foodOrderService = foodOrderService;
    }


    @PutMapping(WebResourceConstant.MyLunch.UPDATE_ORDER_STATUS)
    public ResponseEntity<FortunaResponseObj> update(@RequestBody @Valid OFOUpdateOrderRequestDto updateOrderRequestDto) {
        foodOrderService.updateOrderStatus(updateOrderRequestDto.getOrderId(), updateOrderRequestDto.getOrderStatus());
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().message("Record has been updated.").build(), HttpStatus.OK);
    }


    @PostMapping("/neworder")
    public ResponseEntity<FortunaResponseObj> addNewOrder(@RequestBody @Valid OFOOrderRequestDto dto) {
        OFOFoodOrderEntity entity = (OFOFoodOrderEntity) igqReqbBeanMapper.mapToEntity(dto);

        // setCreateEntityProperties(entity);
        foodOrderService.save(entity);

        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(this.igqResqbBeanMapper.mapToDTO(entity)).message("Record has been created.").build(), HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.MyLunch.CLOSE_SALES)
    public ResponseEntity<FortunaResponseObj> closeSales() {
        this.foodOrderService.closeSales();
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().message("Success").build(), HttpStatus.OK);
    }





}
