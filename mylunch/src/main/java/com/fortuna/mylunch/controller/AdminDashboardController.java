package com.fortuna.mylunch.controller;


import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.dto.mylunch.responseDto.OFOOrderCountResponseDto;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.mylunch.enums.DeliveryType;
import com.fortuna.mylunch.enums.OrderStatus;
import com.fortuna.mylunch.service.IOFOFoodOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by anil on 5/14/18.
 */
@RestController
@RequestMapping(AdminDashboardController.BASE_URL)
public class AdminDashboardController {
    public static final String BASE_URL = WebResourceConstant.MyLunch.ADMIN_DASHBOARD;

    private IOFOFoodOrderService foodOrderService;

    @Autowired
    public AdminDashboardController(IOFOFoodOrderService foodOrderService) {
        this.foodOrderService = foodOrderService;
    }


    @GetMapping(WebResourceConstant.MyLunch.DASHBOARD_ORDER_COUNT)
    public ResponseEntity<FortunaResponseObj> orderCount() {

        OFOOrderCountResponseDto ofoOrderCountResponseDto = new OFOOrderCountResponseDto();
        ofoOrderCountResponseDto.setTotalOrder(foodOrderService.count());
        Long totalOrderByDeliverStatus = foodOrderService.countByOrderStatus(OrderStatus.DELIVERED.getOrderStatus());
        Long totalPaidOrder = foodOrderService.countByOrderStatus(OrderStatus.PAID.getOrderStatus());
        ofoOrderCountResponseDto.setTotalSales(totalOrderByDeliverStatus + totalPaidOrder);
        ofoOrderCountResponseDto.setTotalDelivered(foodOrderService.countByDeliveryType(DeliveryType.HOME_DELIVERY.getDeliveryType()));
        ofoOrderCountResponseDto.setTotalPickWhenReady(foodOrderService.countByDeliveryType(DeliveryType.PICK_WHEN_READY.getDeliveryType()));

        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(ofoOrderCountResponseDto).message("Success").build(), HttpStatus.OK);
    }

}
