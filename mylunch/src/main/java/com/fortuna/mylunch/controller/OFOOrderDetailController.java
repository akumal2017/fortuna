package com.fortuna.mylunch.controller;


import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.controller.FortunaControllerBase;
import com.fortuna.core.dto.mylunch.requestDto.OFOOrderDetailRequestDto;
import com.fortuna.core.dto.mylunch.responseDto.OFOOrderDetailResponseDto;
import com.fortuna.core.exception.FortunaException;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.core.utils.impl.FortunaBeanMapperImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailEntity;
import com.fortuna.mylunch.service.IOFOFoodOrderDetailService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Anil on 5/13/2018.
 */
@RestController
@RequestMapping(OFOOrderDetailController.BASE_URL)
public class OFOOrderDetailController extends FortunaControllerBase {
    public static final String BASE_URL = WebResourceConstant.MyLunch.ORDER_DETAIL;
    private IOFOFoodOrderDetailService foodOrderDetailService;

    public OFOOrderDetailController(IOFOFoodOrderDetailService foodOrderDetailService) {
        super(foodOrderDetailService, new FortunaBeanMapperImpl(OFOFoodOrderDetailEntity.class, OFOOrderDetailRequestDto.class), new FortunaBeanMapperImpl(OFOFoodOrderDetailEntity.class, OFOOrderDetailResponseDto.class));
        this.foodOrderDetailService = foodOrderDetailService;
    }


    @GetMapping(WebResourceConstant.MyLunch.ORDER_ID + WebResourceConstant.GET)
    public ResponseEntity<FortunaResponseObj> getByOrderId(@PathVariable Long orderId) {
        List<OFOFoodOrderDetailEntity> foodOrderDetailEntityList = this.foodOrderDetailService.getListByOrderId(orderId);
        if (foodOrderDetailEntityList.size() == 0) {
            throw new FortunaException("Sorry!! No Records Found");
        }
        return new ResponseEntity<>(new FortunaResponseObj.FortunaResponseObjBuilder().result(igqResqbBeanMapper.mapToDTO(foodOrderDetailEntityList)).message("Success").build(), HttpStatus.OK);
    }


}
