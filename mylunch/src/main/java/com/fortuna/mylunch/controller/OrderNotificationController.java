package com.fortuna.mylunch.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by admin on 5/27/2018.
 */
@RestController
public class OrderNotificationController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public OrderNotificationController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/send/order")
    public void onReceiveMessage(String ofoNewOrderRequest) {
        System.out.println("message = " + ofoNewOrderRequest.toString());
        this.simpMessagingTemplate.convertAndSend("/order", ofoNewOrderRequest);
    }
}
