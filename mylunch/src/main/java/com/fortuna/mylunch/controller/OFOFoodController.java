package com.fortuna.mylunch.controller;


import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.controller.FortunaControllerBase;
import com.fortuna.core.dto.mylunch.requestDto.OFOFoodRequestDto;
import com.fortuna.core.dto.mylunch.responseDto.OFOFoodResponseDto;
import com.fortuna.core.utils.impl.FortunaBeanMapperImpl;
import com.fortuna.database.entity.mylunch.OFOFoodEntity;
import com.fortuna.mylunch.service.IOFOFoodService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Anil on 5/13/2018.
 */
@RestController
@RequestMapping(OFOFoodController.BASE_URL)
public class OFOFoodController extends FortunaControllerBase {
    public static final String BASE_URL = WebResourceConstant.MyLunch.FOOD;

    public OFOFoodController(IOFOFoodService iofoSequenceService) {
        super(iofoSequenceService, new FortunaBeanMapperImpl(OFOFoodEntity.class, OFOFoodRequestDto.class), new FortunaBeanMapperImpl(OFOFoodEntity.class, OFOFoodResponseDto.class));
    }
}
