package com.fortuna.mylunch.controller;

import com.fortuna.core.constant.WebResourceConstant;
import com.fortuna.core.model.FileInfoModel;
import com.fortuna.core.model.FortunaResponseObj;
import com.fortuna.core.utils.FortunaGlobalSettingUtils;
import com.fortuna.core.utils.MultiPartFileUtils;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.util.UUID;

/**
 * Created by admin on 5/23/2018.
 */
@RestController
@RequestMapping(OFOImageUploadController.BASE_URL)
public class OFOImageUploadController {
    public static final String BASE_URL = WebResourceConstant.MyLunch.MY_LUNCH;

    @Autowired
    private HttpServletRequest httpServletRequest;

    public OFOImageUploadController(HttpServletRequest httpServletRequest) {
    }


    @PostMapping(WebResourceConstant.UPLOAD)
    public ResponseEntity<FortunaResponseObj> singleFileUpload(@RequestParam("uploadFile") MultipartFile uploadFile) {

        FileInfoModel fileInfoModel = new FileInfoModel.FileInfoBuilder()
                .folderName(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.IMAGE_UPLOAD_LOCATION))
                .rootLocation(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.ROOT_UPLOAD_LOCATION))
                .multipartFile(uploadFile)
                .build();
        String newFilename = this.getRandomName();
        MultiPartFileUtils.writeandRenameFile(fileInfoModel, newFilename);
        FortunaResponseObj fortunaResponseObj = new FortunaResponseObj.FortunaResponseObjBuilder().result(newFilename).message("success").build();
        return new ResponseEntity<>(fortunaResponseObj, HttpStatus.OK);
    }

    @GetMapping(WebResourceConstant.DISPLAY_FILE)
    public ResponseEntity<byte[]> displayFile(@PathVariable String fileName) throws FileNotFoundException {
        HttpHeaders httpHeaders = new HttpHeaders();
//        System.out.println("image = " + image);
        System.out.println("fileName = " + fileName);

        FileInfoModel fileInfoModel = new FileInfoModel.
                FileInfoBuilder().image(fileName)
                .folderName(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.IMAGE_UPLOAD_LOCATION))
                .rootLocation(FortunaGlobalSettingUtils.getGlobalSettingByKey(FortunaGlobalSettingUtils.ROOT_UPLOAD_LOCATION))
                .build();
        return new ResponseEntity<>(MultiPartFileUtils.readFile(fileInfoModel), httpHeaders, HttpStatus.OK);

    }

    private String getRandomName() {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return randomUUIDString;
    }
    

}
