package com.fortuna.mylunch.enums;

import lombok.Getter;

/**
 * Created by anil on 5/10/18.
 */
@Getter
public enum OrderStatus {

    NEW("NEW"),
    DELIVERED("DELIVERED"),
    PAID("PAID"),
    CANCELED("CANCELED"),
    IN_PROGRESS("IN_PROGRESS");


    private String orderStatus;

    OrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }


}
