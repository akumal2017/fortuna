package com.fortuna.mylunch.enums;

import lombok.Getter;

/**
 * Created by anil on 5/10/18.
 */
@Getter
public enum DeliveryType {


    HOME_DELIVERY("HOME_DELIVERY"),
    PICK_WHEN_READY("PICK_WHEN_READY");

    private String deliveryType;

    DeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }


}
