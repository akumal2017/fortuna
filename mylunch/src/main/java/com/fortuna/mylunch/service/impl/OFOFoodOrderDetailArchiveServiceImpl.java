package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailArchiveEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderDetailArchiveRepository;
import com.fortuna.mylunch.service.IOFOFoodOrderDetailArchiveService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
@Service
public class OFOFoodOrderDetailArchiveServiceImpl extends CrudServiceImpl<OFOFoodOrderDetailArchiveEntity, Long> implements IOFOFoodOrderDetailArchiveService {
    private IOFOFoodOrderDetailArchiveRepository iofoFoodOrderDetailArchiveRepository;

    public OFOFoodOrderDetailArchiveServiceImpl(IOFOFoodOrderDetailArchiveRepository iofoFoodOrderDetailArchiveRepository) {
        super(iofoFoodOrderDetailArchiveRepository);
        this.iofoFoodOrderDetailArchiveRepository = iofoFoodOrderDetailArchiveRepository;
    }

    @Override
    public List<OFOFoodOrderDetailArchiveEntity> getListByOrderId(Long orderId) {
        return iofoFoodOrderDetailArchiveRepository.getListByOrderId(orderId);
    }


}
