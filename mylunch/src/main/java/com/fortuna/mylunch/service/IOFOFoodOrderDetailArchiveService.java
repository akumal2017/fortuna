package com.fortuna.mylunch.service;

import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailArchiveEntity;

import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
public interface IOFOFoodOrderDetailArchiveService extends ICrudService<OFOFoodOrderDetailArchiveEntity, Long> {
    List<OFOFoodOrderDetailArchiveEntity> getListByOrderId(Long orderId);

}
