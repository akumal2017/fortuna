package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.mylunch.OFOSequenceEntity;
import com.fortuna.database.repository.mylunch.IOFOSequenceRepository;
import com.fortuna.mylunch.service.IOFOSequenceService;
import org.springframework.stereotype.Service;

/**
 * Created by admin on 5/23/2018.
 */
@Service
public class OFOSequenceServiceImpl extends CrudServiceImpl<OFOSequenceEntity, Long> implements IOFOSequenceService {
    private IOFOSequenceRepository iofoSequenceRepository;

    public OFOSequenceServiceImpl(IOFOSequenceRepository iofoSequenceRepository) {
        super(iofoSequenceRepository);
        this.iofoSequenceRepository = iofoSequenceRepository;
    }

    @Override
    public OFOSequenceEntity getByName(String name) {
        return iofoSequenceRepository.getByName(name);
    }
}
