package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderDetailRepository;
import com.fortuna.mylunch.service.IOFOFoodOrderDetailService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
@Service
public class OFOFoodOrderDetailServiceImpl extends CrudServiceImpl<OFOFoodOrderDetailEntity, Long> implements IOFOFoodOrderDetailService {
    private IOFOFoodOrderDetailRepository iofoFoodOrderDetailRepository;

    public OFOFoodOrderDetailServiceImpl(IOFOFoodOrderDetailRepository iofoFoodOrderDetailRepository) {
        super(iofoFoodOrderDetailRepository);
        this.iofoFoodOrderDetailRepository = iofoFoodOrderDetailRepository;
    }

    @Override
    public List<OFOFoodOrderDetailEntity> getListByOrderId(Long orderId) {
        return iofoFoodOrderDetailRepository.getListByOrderId(orderId);
    }

    @Override
    public Integer clearTableData() {
        return this.iofoFoodOrderDetailRepository.clearTableData();
    }
}
