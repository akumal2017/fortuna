package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.core.utils.FortunaDateUtils;
import com.fortuna.core.utils.StringUtils;
import com.fortuna.database.entity.mylunch.*;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderRepository;
import com.fortuna.mylunch.enums.OrderStatus;
import com.fortuna.mylunch.service.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
@Transactional
@Service
public class OFOFoodOrderServiceImpl extends CrudServiceImpl<OFOFoodOrderEntity, Long> implements IOFOFoodOrderService {
    private static String SEQUENCE_KEY = "FOOD_ORDER";
    private static Long initialSequence = 0L;
    private static ModelMapper modelMapper = new ModelMapper();
    private IOFOFoodOrderRepository foodOrderRepository;
    private IOFOSequenceService sequenceService;
    private IOFOFoodOrderDetailService foodOrderDetailService;
    private IOFOFoodOrderArchiveService iofoFoodOrderArchiveService;
    private IOFOFoodOrderDetailArchiveService iofoFoodOrderDetailArchiveService;

    public OFOFoodOrderServiceImpl(IOFOFoodOrderRepository iofoFoodOrderRepository,
                                   IOFOSequenceService sequenceService,
                                   IOFOFoodOrderDetailService foodOrderDetailService,
                                   IOFOFoodOrderArchiveService iofoFoodOrderArchiveService,
                                   IOFOFoodOrderDetailArchiveService iofoFoodOrderDetailArchiveService) {
        super(iofoFoodOrderRepository);
        this.foodOrderRepository = iofoFoodOrderRepository;
        this.sequenceService = sequenceService;
        this.foodOrderDetailService = foodOrderDetailService;
        this.iofoFoodOrderArchiveService = iofoFoodOrderArchiveService;
        this.iofoFoodOrderDetailArchiveService = iofoFoodOrderDetailArchiveService;
    }

    @Transactional
    @Override
    public OFOFoodOrderEntity save(OFOFoodOrderEntity ofoFoodOrderEntity) {

        OFOSequenceEntity ofoSequenceEntity = getFoodOrderSequence();
        Long codeSequence = ofoSequenceEntity.getSequenceValue() + 1;
        ofoFoodOrderEntity.setCode(
                this.getNewCode(ofoSequenceEntity, codeSequence)
                        .concat("-" + FortunaDateUtils.today().getYear())
                        .concat("-" + FortunaDateUtils.today().getMonthValue())
                        .concat("-" + FortunaDateUtils.today().getDayOfMonth())
        );
        ofoFoodOrderEntity.setOrderStatus(OrderStatus.NEW.getOrderStatus());
        super.save(ofoFoodOrderEntity);
        ofoSequenceEntity.setSequenceValue(codeSequence);
        sequenceService.update(ofoSequenceEntity);
        this.updateOrderDetails(ofoFoodOrderEntity, ofoFoodOrderEntity.getFoodOrderDetailList());
        return ofoFoodOrderEntity;
    }

    private OFOSequenceEntity getFoodOrderSequence() {
        return sequenceService.getByName(SEQUENCE_KEY);

    }

    private String getNewCode(OFOSequenceEntity ofoSequenceEntity, Long CodeSequence) {
        String preffix = StringUtils.isNotNull(ofoSequenceEntity.getPrefix()) ? ofoSequenceEntity.getPrefix() : "";
        String suffix = StringUtils.isNotNull(ofoSequenceEntity.getSuffix()) ? ofoSequenceEntity.getSuffix() : "";
        String newCode = preffix + CodeSequence + suffix;
        return newCode.trim();
    }

    @Override
    public Long updateOrderStatus(Long orderId, String orderStatus) {
        return foodOrderRepository.updateOrderStatus(orderId, orderStatus);
    }

    @Override
    public Long countByDeliveryType(String deliveryType) {
        return foodOrderRepository.countByDeliveryType(deliveryType);
    }

    @Override
    public Long countByOrderStatus(String orderStatus) {
        return foodOrderRepository.countByOrderStatus(orderStatus);
    }

    private void updateOrderDetails(OFOFoodOrderEntity ofoFoodOrderEntity, List<OFOFoodOrderDetailEntity> orderDetailEntityList) {
        for (OFOFoodOrderDetailEntity orderDetailEntity : orderDetailEntityList) {
            orderDetailEntity.setOfoFoodOrderEntity(ofoFoodOrderEntity);
            orderDetailEntity.setOfoFoodEntity(this.getNewFoodEntity(orderDetailEntity.getOfoFoodEntity()));
            foodOrderDetailService.save(orderDetailEntity);

        }
    }

    private OFOFoodEntity getNewFoodEntity(OFOFoodEntity ofoFoodEntity) {
        OFOFoodEntity newFoodEntity = new OFOFoodEntity();
        newFoodEntity.setId(ofoFoodEntity.getId());
        newFoodEntity.setName(ofoFoodEntity.getName());
        newFoodEntity.setPrice(ofoFoodEntity.getPrice());
        return newFoodEntity;
    }

    @Transactional
    @Override
    public Boolean closeSales() {
        List<OFOFoodOrderEntity> foodOrderEntities = findAll();
        //move to archive
        if (!foodOrderEntities.isEmpty()) {
            for (OFOFoodOrderEntity ofoFoodOrderEntity : foodOrderEntities) {
                OFOFoodOrderArchiveEntity ofoFoodOrderArchiveEntity = mapToFoodOrderArchiveEntity(ofoFoodOrderEntity);
                ofoFoodOrderArchiveEntity.setId(null);
                iofoFoodOrderArchiveService.save(ofoFoodOrderArchiveEntity);
                updateOrderArchiveDetails(ofoFoodOrderArchiveEntity, ofoFoodOrderArchiveEntity.getFoodOrderDetailList());
            }
            //clear order table
            this.foodOrderRepository.clearTableData();
            //clear order detail table
            this.foodOrderDetailService.clearTableData();
            //reset sequence value
            OFOSequenceEntity ofoSequenceEntity = getFoodOrderSequence();
            ofoSequenceEntity.setSequenceValue(initialSequence);
            this.sequenceService.update(ofoSequenceEntity);
        }
        return true;
    }

    private OFOFoodOrderArchiveEntity mapToFoodOrderArchiveEntity(OFOFoodOrderEntity ofoFoodOrderEntity) {
        return modelMapper.map(ofoFoodOrderEntity, OFOFoodOrderArchiveEntity.class);
    }

    private void updateOrderArchiveDetails(OFOFoodOrderArchiveEntity ofoFoodOrderArchiveEntity, List<OFOFoodOrderDetailArchiveEntity> orderDetailArchiveEntityList) {
        if (!orderDetailArchiveEntityList.isEmpty()) {
            for (OFOFoodOrderDetailArchiveEntity orderDetailArchiveEntity : orderDetailArchiveEntityList) {
                orderDetailArchiveEntity.setId(null);
                orderDetailArchiveEntity.setOfoFoodOrderEntity(ofoFoodOrderArchiveEntity);
                orderDetailArchiveEntity.setOfoFoodEntity(this.getNewFoodEntity(orderDetailArchiveEntity.getOfoFoodEntity()));
                iofoFoodOrderDetailArchiveService.save(orderDetailArchiveEntity);
            }
        }
    }



}
