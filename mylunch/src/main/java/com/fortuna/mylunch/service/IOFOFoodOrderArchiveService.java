package com.fortuna.mylunch.service;

import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.mylunch.OFOFoodOrderArchiveEntity;

/**
 * Created by admin on 5/23/2018.
 */
public interface IOFOFoodOrderArchiveService extends ICrudService<OFOFoodOrderArchiveEntity, Long> {
    Long updateOrderStatus(Long orderId, String orderStatus);

    Long countByDeliveryType(String deliveryType);

    Long countByOrderStatus(String orderStatus);
}
