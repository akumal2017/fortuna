package com.fortuna.mylunch.service;

import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.mylunch.OFOSequenceEntity;

/**
 * Created by admin on 5/23/2018.
 */
public interface IOFOSequenceService extends ICrudService<OFOSequenceEntity, Long> {
    OFOSequenceEntity getByName(String name);
}
