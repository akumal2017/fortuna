package com.fortuna.mylunch.service;

import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.mylunch.OFOFoodOrderDetailEntity;

import java.util.List;

/**
 * Created by admin on 5/23/2018.
 */
public interface IOFOFoodOrderDetailService extends ICrudService<OFOFoodOrderDetailEntity, Long> {
    List<OFOFoodOrderDetailEntity> getListByOrderId(Long orderId);

    Integer clearTableData();
}
