package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.database.entity.mylunch.OFOFoodOrderArchiveEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodOrderArchiveRepository;
import com.fortuna.mylunch.service.IOFOFoodOrderArchiveService;
import com.fortuna.mylunch.service.IOFOFoodOrderDetailArchiveService;
import com.fortuna.mylunch.service.IOFOSequenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/23/2018.
 */
@Transactional
@Service
public class OFOFoodOrderArchiveServiceImpl extends CrudServiceImpl<OFOFoodOrderArchiveEntity, Long> implements IOFOFoodOrderArchiveService {
    private static String SEQUENCE_KEY = "FOOD_ORDER";
    private IOFOFoodOrderArchiveRepository foodOrderArchiveRepository;
    private IOFOSequenceService sequenceService;
    private IOFOFoodOrderDetailArchiveService foodOrderDetailArchiveService;

    public OFOFoodOrderArchiveServiceImpl(IOFOFoodOrderArchiveRepository iofoFoodOrderArchiveRepository,
                                          IOFOSequenceService sequenceService,
                                          IOFOFoodOrderDetailArchiveService foodOrderDetailArchiveService) {
        super(iofoFoodOrderArchiveRepository);
        this.foodOrderArchiveRepository = iofoFoodOrderArchiveRepository;
        this.sequenceService = sequenceService;
        this.foodOrderDetailArchiveService = foodOrderDetailArchiveService;
    }

    @Override
    public Long updateOrderStatus(Long orderId, String orderStatus) {
        return foodOrderArchiveRepository.updateOrderStatus(orderId, orderStatus);
    }

    @Override
    public Long countByDeliveryType(String deliveryType) {
        return foodOrderArchiveRepository.countByDeliveryType(deliveryType);
    }

    @Override
    public Long countByOrderStatus(String orderStatus) {
        return foodOrderArchiveRepository.countByOrderStatus(orderStatus);
    }


}
