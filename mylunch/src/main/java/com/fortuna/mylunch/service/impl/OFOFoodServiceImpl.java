package com.fortuna.mylunch.service.impl;

import com.fortuna.core.service.impl.CrudServiceImpl;
import com.fortuna.core.utils.StringUtils;
import com.fortuna.database.entity.mylunch.OFOFoodEntity;
import com.fortuna.database.entity.mylunch.OFOSequenceEntity;
import com.fortuna.database.repository.mylunch.IOFOFoodRepository;
import com.fortuna.mylunch.service.IOFOFoodService;
import com.fortuna.mylunch.service.IOFOSequenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by admin on 5/23/2018.
 */
@Transactional
@Service
public class OFOFoodServiceImpl extends CrudServiceImpl<OFOFoodEntity, Long> implements IOFOFoodService {
    private static String SEQUENCE_KEY = "FOOD";
    private IOFOSequenceService sequenceService;
    private IOFOFoodRepository foodRepository;

    public OFOFoodServiceImpl(IOFOFoodRepository iofoFoodRepository, IOFOSequenceService sequenceService) {
        super(iofoFoodRepository);
        this.sequenceService = sequenceService;
        this.foodRepository = iofoFoodRepository;
    }

    private OFOSequenceEntity getFoodSequence() {
        return sequenceService.getByName(SEQUENCE_KEY);

    }

    private String getNewCode(OFOSequenceEntity ofoSequenceEntity, Long CodeSequence) {
        String preffix = StringUtils.isNotNull(ofoSequenceEntity.getPrefix()) ? ofoSequenceEntity.getPrefix() : "";
        String suffix = StringUtils.isNotNull(ofoSequenceEntity.getSuffix()) ? ofoSequenceEntity.getSuffix() : "";
        String newCode = preffix + CodeSequence + suffix;
        return newCode.trim();
    }

    @Override
    public OFOFoodEntity save(OFOFoodEntity ofoFoodEntity) {
        OFOSequenceEntity ofoSequenceEntity = getFoodSequence();
        Long codeSequence = ofoSequenceEntity.getSequenceValue() + 1;
        ofoFoodEntity.setCode(this.getNewCode(ofoSequenceEntity, codeSequence));
        super.save(ofoFoodEntity);
        ofoSequenceEntity.setSequenceValue(codeSequence);
        sequenceService.update(ofoSequenceEntity);
        return ofoFoodEntity;
    }

    @Override
    public OFOFoodEntity update(OFOFoodEntity entity) {
        OFOFoodEntity existingEntity = foodRepository.findOne(entity.getId());
        entity.setCode(existingEntity.getCode());
        return super.update(entity);
    }
}
