package com.fortuna.mylunch.service;

import com.fortuna.core.service.ICrudService;
import com.fortuna.database.entity.mylunch.OFOFoodEntity;

/**
 * Created by admin on 5/23/2018.
 */
public interface IOFOFoodService extends ICrudService<OFOFoodEntity, Long> {
}
